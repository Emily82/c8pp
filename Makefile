# Makefile for linux environment

CXX = g++

DEBUG =

INCLUDES = -I./include/
LIBRARIES = -L/usr/lib -L/usr/local/lib

CXXFLAGS = -Wall -g -std=c++17 $(DEBUG) $(INCLUDES)
LDFLAGS = -lSDL2

TARGET = c8pp

DBG_TARGET = dbgc8pp

SOURCE = src/c8pp.cpp \
	 src/video.cpp \
	 src/videoSDL.cpp \
	 src/utils.cpp
OBJECTS = ${SOURCE:%.cpp=%.o}

all: $(TARGET)
debug: $(DBG_TARGET)

$(TARGET): src/main.o $(OBJECTS)
	$(CXX) -o $@ $^ $(LDFLAGS) $(LIBRARIES)
	-rm -f src/main.o $(OBJECTS)

$(DBG_TARGET): CXXFLAGS += -D DEBUG_DISASM
$(DBG_TARGET): src/dbgmain.o src/shell.o $(OBJECTS)
	$(CXX) -o $@ $^ $(LDFLAGS) $(LIBRARIES)
	-rm -f src/dbgmain.o src/shell.o $(OBJECTS)

.PHONY : clean

clean:
	-rm -f src/main.o src/dbgmain.o src/shell.o $(TARGET) $(DBG_TARGET) $(OBJECTS)
