;testing some instructions :)
	jp main
;v0: font char to print
;v1: x coordinate
;v2: y coordinate
putchar:
	ld	f,	v0
	drw	v1,	v2,	#5
	ret
;i : sprite address
;v1: x coordinate
;v2: y coordinate
putsprite:
	drw	v1,	v2,	#00
	ret
;v1: timer delay ticks per scroll
scrolldown:
	ld  dt, v1
	scd	#04
	ret
;v1: timer delay ticks per scroll
scrollright:
	ld	dt,	v1
	scr
	ret
;v1: timer delay ticks per scroll
scrollleft:
	ld	dt,	v1
	scl
	ret
main:
	high
	;print F char
	ld	v0,	#0F
	ld	v1,	#32 ; x
	ld	v2,	#12 ; y
	call putchar
	;print A char
	ld  v0, #0A
	ld	v1,	#0a ; x
	ld	v2,	#0a ; y
	call putchar
	;draw quad sprite
	ld	i,	sprite
	ld	v1,	#00 ; x
	ld	v2,	#00 ; y
	call putsprite
input:
	ld 	v0, k
	ld  v1, #05
	sne	v0,	#04
	call scrollleft ; key 4 to scroll left
	sne	v0,	#05
	call scrolldown ; key 5 to scroll down
	sne	v0,	#06
	call scrollright ; key 6 to scroll right
	jp input
sprite:
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
	db	#FF, #FF
end:
	jp end
