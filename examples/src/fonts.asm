; test code to write fonts in Chip8 assembly
	jp main
print_font:
;v0 : number to show
;v1 : x coordinate
;v2 : y coordinate
	ld f, v0
	drw v1, v2, #5
	ret
main:
	ld v0, #00
	ld v1, #00
	ld v2, #00
loop:
	call print_font
	add v0, #01
	add v1, #08
	se v0, #08
	jp loop
	add v2, #06
	ld v1, #00
new_loop:
	call print_font
	add v0, #01
	add v1, #08
	se v0, #10
	jp new_loop
end:
	jp end
