	ld 	v0, #00	; x position for movable sprite
	ld 	v1, #00 ; y position for movable sprite
	ld 	v2, #01 ; movable sprite
	ld	v3,	#0F ; x position for fixed sprite
	ld	v4,	#0E ; fixed sprite
	ld	f, 	v4
	drw	v3,	v1, 5 ; draw fixed sprite
	ld 	f, 	v2
	drw	v0, v1, 5 ; draw movable sprite
loop:
	drw	v0, v1, 5
	sknp v2 ; press 1 to move the sprite otherwise skip
	add	v0,	2 ; move the sprite 2 pixels right
	drw	v0, v1, 5
	ld	st,	vf
	jp loop
