#include <utils.h>
#include <iostream>
#include <getopt.h>
#include <cstring>
#include <c8pp.h>
#include <video.h>

#include <dirent.h>
#include <sys/types.h>

uint32_t sleepDelay = DEFAULT_SLEEP_DELAY; //microseconds

//Here we handle user commandline options
int optSetup(int argc, char *argv[], Chip8 *myChip8, Video *gui)
{
	int c = 0;
	bool rOpt = false;
	while ((c = getopt(argc, argv, "r:q:s:c:f")) != -1) {
		int index = 0;
		char *param = NULL;
		float multiplier;
		float cpuFreq;
		switch (c)
		{
			case 'r':
				if (fileExistsAndNotDir(optarg)) {
					rOpt = true;
					myChip8->load(optarg);
				}
				else
					cout << "Error: file " << optarg << " does not exist" << endl;
			break;
			case 'q':
				for (index = (optind - 1) ; index < argc ; ++index) {
					param = argv[index];
					if (param[0] == '-') /* check if all arguments are read */
						break;
					if (strcmp(param, "loadstore") == 0)
						myChip8->loadStoreQuirk = true;
					else if (strcmp(param, "shift") == 0)
						myChip8->shiftQuirk = true;
					else if (strcmp(param, "packedbcd") == 0)
						myChip8->packedBcdQuirk = true;
					else if (strcmp(param, "imagewrap") == 0)
						myChip8->imageWrapQuirk = true;
					else {
						fprintf (stderr, "Unknown parameters for option -q\n");
						return ERR;
					}
				}
				optind = index - 1;
			break;
			case 's':
				gui->setScale(atoi(optarg));
			break;
			case 'c':
				//Clock multiplier default value: 1 (1000 * 1 Hz)
				multiplier = atof(optarg);
				cpuFreq = BASE_FREQ * multiplier;
				sleepDelay = STOUS(1 / cpuFreq);
			break;
			case 'f':
				gui->enableFullScreen();
			break;
			//TODO: make code for -C option to setup emulator color
			case '?':
				if (optopt == 'c')
					fprintf (stderr, "Option -%c requires an argument.\n", optopt);
				else if (isprint (optopt))
					fprintf (stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf (stderr,
							"Unknown option character `\\x%x'.\n",
							optopt);
				return ERR;
			break;
			default:
				cout << "Usage: chip8 -r program.c8" << endl;
				return ERR;
		}
	}
	if (!rOpt) {
		cout << "No ROM specified, booting C8pp bios ..." << endl;
		return NOROM;
	}
	return NOERR;
}

//Check if file exists and if it is NOT a directory
bool fileExistsAndNotDir(const char *filename)
{
	bool exists = false;

	ifstream ifile(filename);
	if (ifile) {
		//We need to check if this file is a directory, if so then file does not exists
		DIR *dirTest;
		dirTest = opendir(filename);
		if (!dirTest)
			exists = true;
	}
	ifile.close();

	return exists;
}

void checkCpuExceptions(Chip8 *myChip8, int errorCode) {
	switch (errorCode) {
		case EXC_UNKNOWN_OPCODE:
			cout << "CPU exception, unknown opcode" << endl;
			myChip8->dumpCpuStatus();
		break;
		case EXC_EMPTY_STACK:
			cout << "CPU exception, empty stack" << endl;
			myChip8->dumpCpuStatus();
		break;
		case EXC_FULL_STACK:
			cout << "CPU exception, full stack" << endl;
			myChip8->dumpCpuStatus();
		break;
		case EXC_OUT_OF_MEMORY:
			cout << "CPU exception, address out of memory" << endl;
			myChip8->dumpCpuStatus();
		default:
			cout << "Bye!" << endl;
		break;
	}
}
