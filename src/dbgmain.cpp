#include <c8pp.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdint>
#include <videoSDL.h>
#include <chrono>
#include <ctime>
#include <getopt.h>
#include <string>
#include <utils.h>
#include <vector>
#include <shell.h>
#ifdef _WIN32
	#include <windows.h>
#endif

using namespace std;

int main(int argc, char **argv) {
	VideoSDL gui(DEFAULT_GFX_WIDTH, DEFAULT_GFX_HEIGHT);
	Chip8 myChip8(&gui);
	int error = 0;

	myChip8.initialize();
	gui.initSDL();

	int optionResult = optSetup(argc, argv, &myChip8, &gui);
	if (optionResult == ERR)
		return 1;
	else if (optionResult == NOROM)
		myChip8.load(FAKEBIOS, true);

	//Entire loop time and timers time
	TimePoint start, end, timerStart, timerEnd;
	//Clock cycle only time
	TimePoint cycleStart, cycleEnd;
	uint32_t timersTime;
	uint8_t *vramAccess = myChip8.getGfx();

	//Breakpoint address input
	gui.hideScreen();
	uint16_t currentAddr = 0;
	Shell shDbg(&myChip8, &gui);
	uint16_t debugSignal = shDbg.dbgShell(0);
	gui.showScreen();

	//we start to get time for timers (till 0.016 seconds, 60hz)
	timerStart = NOW();
	//Emulation loop
	while (!gui.onPowerOff() && debugSignal != QUIT)
	{
		currentAddr = myChip8.getPc();
		bool breakpointStop = shDbg.checkAddressBreakpoints();
		if (!breakpointStop) {
			//We can set breakpointStop also with a step debug command
			breakpointStop = (debugSignal == STEP)? true : false;

			if (!breakpointStop)
				breakpointStop = shDbg.checkConditionBreakpoints();
		}

		#ifdef DEBUG_SHOW_CPU_FREQ
			start = NOW();
		#endif
		//Check soft reset request
		if (gui.onReset())
			myChip8.reset();

		//Handle emulator pause
		if (!gui.onPause())
		{
			cycleStart = NOW();
			timerEnd = NOW();
			//We check if time exceeded 16 ms to update timers
			timersTime = DURATION(timerStart, timerEnd);
			if (timersTime >= TIMERS_DELAY) {
				//Handle user input events
				gui.inputEvents();
				myChip8.updateTimers();

				timerStart = NOW(); //we reset timerStart
			}

			//Screen update only for debugging must be done for every clock cycle and not 60fps
			if (gui.drawFlag) {
				gui.updateScreen(vramAccess);
				gui.drawFlag = false;
			}

			error = myChip8.emulateCycle();

			#ifdef DEBUG_DUMPCPU
				myChip8.dumpCpuStatus();
			#endif
			#ifdef DEBUG_MEM
				myChip8.dumpMem();
			#endif
			#ifdef DEBUG_VMEM
				myChip8.dumpVmem();
			#endif

			if (error)
				break;

			cycleEnd = NOW();

			if (breakpointStop) {//SHELL STOP
				/*we need to keep the same time interval between timerStart and timerEnd while the emulator is paused*/
				uSeconds interval = INTERVAL(timerStart, timerEnd);

				//Print disasm of last opcode if step
				if (debugSignal == STEP)
					cout << myChip8.getDisasm();

				debugSignal = shDbg.dbgShell(currentAddr); //blocking

				timerEnd = NOW();
				timerStart = (timerEnd - interval);
			}
		}
		else {//PAUSE
			/*we need to keep the same time interval between timerStart and timerEnd while the emulator is paused*/
			uSeconds interval = INTERVAL(timerStart, timerEnd);

			gui.inputEvents(); //wait keyboard event to resume (non-blocking)

			timerEnd = NOW();
			timerStart = (timerEnd - interval);
		}

		#ifndef _WIN32
			//For linux
			//We need to adjust the sleep according to cycle time
			//Cycle time isnt always the same, it can be big or little delay
			uint32_t cycleTime = DURATION(cycleStart, cycleEnd);
			timespec delay;
			delay.tv_sec = 0;
			delay.tv_nsec = USTONS((sleepDelay - cycleTime));
			if (delay.tv_nsec > 0)
				nanosleep(&delay, NULL);
		#else
			//For windows we cannot be so accurate
			Sleep(1 + USTOMS(sleepDelay));
		#endif

		#ifdef DEBUG_SHOW_CPU_FREQ
			end = NOW();
			float clockTime = DURATION(start, end);
			float cpuFreq = (1 / USTOS(clockTime));
			cout << "CPU speed: " << uint32_t(cpuFreq) << " Hz" << endl;
		#endif
	}

	//Cpu Runtime Exceptions
	checkCpuExceptions(&myChip8, error);

	return error;
}
