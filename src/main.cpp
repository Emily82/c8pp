#include <c8pp.h>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <videoSDL.h>
#include <chrono>
#include <ctime>
#include <getopt.h>
#include <cstring>
#include <utils.h>
#ifdef _WIN32
	#include <windows.h>
#endif

//Debug features
//#define DEBUG_DUMPCPU
//#define DEBUG_MEM
//#define DEBUG_VMEM
//#define DEBUG_SHOW_CPU_FREQ

using namespace std;

int main(int argc, char **argv) {
	VideoSDL gui(DEFAULT_GFX_WIDTH, DEFAULT_GFX_HEIGHT);
	Chip8 myChip8(&gui);
	int error = 0;

	myChip8.initialize();
	gui.initSDL();

	int optionResult = optSetup(argc, argv, &myChip8, &gui);
	if (optionResult == ERR)
		return 1;
	else if (optionResult == NOROM)
		myChip8.load(FAKEBIOS, true);

	//Entire loop time and timers time
	TimePoint start, end, timerStart, timerEnd;
	//Clock cycle only time
	TimePoint cycleStart, cycleEnd;
	uint32_t timersTime;

	uint8_t *vramAccess = myChip8.getGfx();

	//we start to get time for timers (till 0.016 seconds, 60hz)
	timerStart = NOW();
	//Emulation loop
	while (!gui.onPowerOff())
	{
		#ifdef DEBUG_SHOW_CPU_FREQ
			start = NOW();
		#endif
		//Check soft reset request
		if (gui.onReset())
			myChip8.reset();

		//Handle emulator pause
		if (!gui.onPause())
		{
			cycleStart = NOW();
			timerEnd = NOW();
			//We check if time exceeded 16 ms to update timers
			timersTime = DURATION(timerStart, timerEnd);
			if (timersTime >= TIMERS_DELAY) {
				//Handle user input events
				gui.inputEvents();
				myChip8.updateTimers();
				//Screen update, 60FPS
				if (gui.drawFlag) {
					gui.updateScreen(vramAccess);
					gui.drawFlag = false;
				}

				timerStart = NOW(); //we reset timerStart
			}

			error = myChip8.emulateCycle();

			#ifdef DEBUG_DUMPCPU
				myChip8.dumpCpuStatus();
			#endif
			#ifdef DEBUG_MEM
				myChip8.dumpMem();
			#endif
			#ifdef DEBUG_VMEM
				myChip8.dumpVmem();
			#endif

			if (error)
				break;
			cycleEnd = NOW();
		}
		else {//PAUSE
			/*we need to keep the same time interval between timerStart and timerEnd while the emulator is paused*/
			uSeconds interval = INTERVAL(timerStart, timerEnd);

			gui.inputEvents(); //wait keyboard event to resume (non-blocking)

			timerEnd = NOW();
			timerStart = (timerEnd - interval);
		}		
		#ifndef _WIN32
			//For linux
			//We need to adjust the sleep according to cycle time
			//Cycle time isnt always the same, it can be big or little delay
			uint32_t cycleTime = DURATION(cycleStart, cycleEnd);
			timespec delay;
			delay.tv_sec = 0;
			delay.tv_nsec = USTONS((sleepDelay - cycleTime));
			if (delay.tv_nsec > 0)
				nanosleep(&delay, NULL);
		#else
			//For windows we cannot be so accurate
			Sleep(1 + USTOMS(sleepDelay));
		#endif

		#ifdef DEBUG_SHOW_CPU_FREQ
			end = NOW();
			float clockTime = DURATION(start, end);
			float cpuFreq = (1 / USTOS(clockTime));
			cout << "CPU speed: " << uint32_t(cpuFreq) << " Hz" << endl;
		#endif
	}

	//Cpu Runtime Exceptions
	checkCpuExceptions(&myChip8, error);

	return error;
}

