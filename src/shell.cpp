#include <shell.h>
#include <exception>
#include <algorithm>
#include <regex>
#include <fstream>

//Syntax Regular expression rules
#define SYNTAX_CONT			"^(cont|c)\\s*$"
#define SYNTAX_STEP			"^(step|s)\\s*$"
#define SYNTAX_BREAK		"^(break|b)\\s+(0x)?[\\da-fA-F]{1,3}\\s*$"
#define SYNTAX_BREAKIF_V 	"^(breakif|b)\\s+(V|v)([\\da-fA-F])\\s*=\\s*(0x)?([\\da-fA-F]){1,2}\\s*$"
#define SYNTAX_BREAKIF_R 	"^(breakif|b)\\s+(R|r)([\\da-fA-F])\\s*=\\s*(0x)?([\\da-fA-F]){1,2}\\s*$"
#define SYNTAX_BREAKIF_I	"^(breakif|b)\\s+(I|i)\\s*=\\s*(0x)?([\\da-fA-F]){1,3}\\s*$"
#define SYNTAX_BREAKIF_SP	"^(breakif|b)\\s+(SP|sp)\\s*=\\s*(0x)?([\\da-fA-F]){1,3}\\s*$"
#define SYNTAX_BREAKIF_DT	"^(breakif|b)\\s+(DT|dt)\\s*=\\s*(0x)?([\\da-fA-F]){1,2}\\s*$"
#define SYNTAX_BREAKIF_ST	"^(breakif|b)\\s+(ST|st)\\s*=\\s*(0x)?([\\da-fA-F]){1,2}\\s*$"
#define SYNTAX_BREAKLIST	"^(breaklist|bl)\\s*$"
#define SYNTAX_BREAKIFLIST	"^(breakiflist|bi)\\s*$"
#define SYNTAX_PRINTCPU		"^(printcpu|p)\\s*$"
#define SYNTAX_QUIT			"^(quit|q)\\s*$"
#define SYNTAX_DISASM		"^(disasm|d)\\s*$"
#define SYNTAX_PRINT_M		"^(printm|p)(\\s+(0x)?[\\da-fA-F]{1,3}){1,2}\\s*$"
#define SYNTAX_PRINT_VM		"^(printv|v)(\\s+(0x)?[\\da-fA-F]{1,3}){1,2}\\s*$"
#define SYNTAX_PRINT_VX		"^(print|p)\\s+(v|V)[\\da-fA-F]\\s*$"
#define SYNTAX_PRINT_RX		"^(print|p)\\s+(r|R)[\\da-fA-F]\\s*$"
#define SYNTAX_PRINT_I		"^(print|p)\\s+(i|I)\\s*$"
#define SYNTAX_PRINT_SP		"^(print|p)\\s+(sp|SP)\\s*$"
#define SYNTAX_PRINT_DT		"^(print|p)\\s+(dt|DT)\\s*$"
#define SYNTAX_PRINT_ST		"^(print|p)\\s+(st|ST)\\s*$"
#define SYNTAX_UNBREAK		"^(unbreak|u)\\s*$"
#define SYNTAX_UNBREAKIF_V	"^(unbreakif|u)\\s*(v|V)[\\da-fA-F]\\s*$"
#define SYNTAX_UNBREAKIF_R	"^(unbreakif|u)\\s*(r|R)[\\da-fA-F]\\s*$"
#define SYNTAX_UNBREAKIF_I	"^(unbreakif|u)\\s*(i|I)\\s*$"
#define SYNTAX_UNBREAKIF_SP	"^(unbreakif|u)\\s*(sp|SP)\\s*$"
#define SYNTAX_UNBREAKIF_DT	"^(unbreakif|u)\\s*(dt|DT)\\s*$"
#define SYNTAX_UNBREAKIF_ST	"^(unbreakif|u)\\s*(st|ST)\\s*$"
#define SYNTAX_SET_V		"^(set|s)\\s+(V|v)([\\da-fA-F])\\s*=\\s*(0x)?([\\da-fA-F]){1,2}\\s*$"
#define SYNTAX_SET_R		"^(set|s)\\s+(R|r)([\\da-fA-F])\\s*=\\s*(0x)?([\\da-fA-F]){1,2}\\s*$"
#define SYNTAX_SET_I		"^(set|s)\\s+(I|i)\\s*=\\s*(0x)?([\\da-fA-F]){1,3}\\s*$"
#define SYNTAX_SET_SP		"^(set|s)\\s+(SP|sp)\\s*=\\s*(0x)?([\\da-fA-F]){1,3}\\s*$"
#define SYNTAX_SET_DT		"^(set|s)\\s+(DT|dt)\\s*=\\s*(0x)?([\\da-fA-F]){1,2}\\s*$"
#define SYNTAX_SET_ST		"^(set|s)\\s+(ST|st)\\s*=\\s*(0x)?([\\da-fA-F]){1,2}\\s*$"
#define SYNTAX_POKE			"^(poke|pk)(\\s+(0x)?[\\da-fA-F]{1,3}){1,2}"\
							"\\s*=\\s*(0x)?[\\da-fA-F]{1,2}\\s*$"
#define SYNTAX_VPOKE		"^(vpoke|vpk)(\\s+(0x)?[\\da-fA-F]{1,3}){1,2}"\
							"\\s*=\\s*(0x)?[\\da-fA-F]{1,2}\\s*$"
#define SYNTAX_SETPIXEL		"^(setpixel|setp)(\\s+([\\da-fA_F]){1,3}){2}\\s*$"
#define SYNTAX_CLEARPIXEL	"^(clearpixel|clrp)(\\s+([\\da-fA_F]){1,3}){2}\\s*$"
#define SYNTAX_HELP			"^(help|h)\\s*\\b[a-zA-Z]*\\s*$"

//Get data regular expression rules
#define BREAK_ADDRESS			"(0x)?([\\da-fA-F]){1,3}$"
#define NREG					"([\\da-fA-F])\\s*="
#define REGVAL					"(0x)?([\\da-fA-F]){1,2}$"
#define ADDRESSVAL				"(0x)?([\\da-fA-F]){1,3}$"
#define M_ADDRESS				"\\b(0x)?([\\da-fA-F]){1,3}"
#define COORDINATE				"\\b([\\da-fA_F]){1,2}"
#define XREG					"([\\da-fA-F])$"
#define PARAM					"\\b[a-zA-Z]*$"

#define ND						0xffff  //Not defined value for breakpoint conditions
#define HELPFILE				"dbg.help"
#define	HELP_SEPARATOR			':'


Shell::Shell(Chip8 *myChip8, Video *gui) {
	cout << "------ C8pp debug shell -------" << endl << endl;
	this->myChip8 = myChip8;
	this->gui = gui;
	memset(Vbreakpoints, ND, REGNUM * sizeof(uint16_t));
	memset(Rbreakpoints, ND, R_REGNUM * sizeof(uint16_t));
	Ibreakpoint = ND;
	SPbreakpoint = ND;
	DTbreakpoint = ND;
	STbreakpoint = ND;
}

void Shell::printAddressBreakPoints() {
	cout << "***** Address breakpoints *****" << endl << endl;
	for (uint32_t i = 0 ; i < breakpoints.size() ; ++i)
		cout << "0x" << breakpoints[i] << " ";
	cout << endl;
}

void Shell::printConditionBreakPoints() {
	cout << "***** Condition breakpoints *****" << endl << endl;
	for (uint16_t i = 0 ; i < REGNUM ; ++i) {
		if (Vbreakpoints[i] == ND)
			cout << "V" << hex << i << " = ND" << endl;
		else
			cout << "V" << hex << i << " = " << Vbreakpoints[i] << endl;
	}
	for (uint16_t i = 0 ; i < R_REGNUM ; ++i) {
		if (Rbreakpoints[i] == ND)
			cout << "R" << hex << i << " = ND" << endl;
		else
			cout << "R" << hex << i << " = " << Rbreakpoints[i] << endl;
	}
	if (Ibreakpoint == ND)
		cout << "I = ND" << endl;
	else
		cout << "I = " << hex << Ibreakpoint << endl;

	if (SPbreakpoint == ND)
		cout << "SP = ND" << endl;
	else
		cout << "SP = " << hex << SPbreakpoint << endl;

	if (DTbreakpoint == ND)
		cout << "DT = ND" << endl;
	else
		cout << "DT = " << hex << DTbreakpoint << endl;

	if (STbreakpoint == ND)
		cout << "ST = ND" << endl;
	else
		cout << "ST = " << hex << STbreakpoint << endl;
}

uint16_t Shell::countSpaces(string s) {
	uint8_t count = 0;
	for (uint8_t i = 0; i < s.length(); i++)
	{
		if (isspace(s[i]))
			count++;
	}
	return count;
}

bool Shell::checkConditionBreakpoints() {
	//Check V registers
	for (uint8_t i = 0 ; i < REGNUM ; ++i) {
		if (Vbreakpoints[i] == (myChip8->getV(i)))
			return true;
	}
	//Check R registers
	for (uint8_t i = 0 ; i < R_REGNUM ; ++i) {
		if (Rbreakpoints[i] == (myChip8->getR(i)))
			return true;
	}

	//Chip8 max ram address its 0xfff
	if (Ibreakpoint == myChip8->getI() || SPbreakpoint == myChip8->getSP() ||
		DTbreakpoint == myChip8->getDT() || STbreakpoint == myChip8->getST()) {
		return true;
	}

	return false;
}

bool Shell::checkAddressBreakpoints() {
	uint16_t currentAddr = myChip8->getPc();
	return binary_search(breakpoints.begin(), breakpoints.end(), currentAddr);
}

//Get a number in string s according to regular expression rule
uint16_t Shell::getNumber(string s, regex rule, uint16_t *pos) {
	stringstream ss;
	uint16_t number;
	smatch out;

	regex_search (s, out, rule);

	if (pos != nullptr)
		*pos = out.position() + out.str().length();

	ss << hex << out.str();
	ss >> number;

	return number;
}

//Get a string param from s string according to regular expression rule
string Shell::getParam(string s, regex rule, uint16_t *pos) {
	smatch out;
	regex_search (s, out, rule);

	if (pos != nullptr)
		*pos = out.position() + out.str().length();

	return out.str();
}

uint16_t Shell::countTokens(string s) {
	uint16_t count = 0;
	stringstream ss(cmd);
	while (!ss.eof()) {
		string token;
		ss >> token;
		++count;
	}

	return count;
}

void Shell::printHelp(string command) {
	ifstream helpfile(HELPFILE);
	string line;
	bool found = false;
	if (command == "")
		cout << "Available commands" << endl << endl;
	while (getline(helpfile, line)) {
		istringstream iss(line);
		string cmdToken, explToken, shortToken;
		getline(iss, cmdToken, HELP_SEPARATOR);
		getline(iss, explToken, HELP_SEPARATOR);
		getline(iss, shortToken, HELP_SEPARATOR);

		//Clean tokens from white spaces
		cmdToken = regex_replace( cmdToken, regex("\\s+"), "" );
		//Here just beginning white spaces
		explToken = regex_replace( explToken, regex("^\\s+"), "" );
		shortToken = regex_replace( shortToken, regex("^\\s+"), "" );
		if (command == "") {
			found = true;
			cout << cmdToken << endl;
		}
		else if (command == cmdToken) {
			found = true;
			cout << "****" << cmdToken << "****" << endl << endl;
			cout << explToken << endl;
			cout << "Short form : " << shortToken << endl << endl;
		}
	}
	helpfile.close();
	if (!found)
		cout << "Help: Command not found" << endl;
}

int Shell::dbgShell(uint16_t currentAddr) {
	int resultCmd;
	while (true) {
		cout << "0x" << hex << currentAddr << "> ";
		getline (cin, cmd);
		if (cmd == "") {
			cmd = lastcmd;
		}

		lastcmd = cmd;
		if (regex_match (cmd, regex(SYNTAX_CONT) )) {
			resultCmd = CONT;
			break;
		}
		else if (regex_match (cmd, regex(SYNTAX_STEP) )) {
			resultCmd = STEP;
			break;
		}
		else if (regex_match (cmd, regex(SYNTAX_BREAK) )) {
			regex rule(BREAK_ADDRESS);
			uint16_t address = getNumber(cmd, rule);

			if (/*address == 0 || address < START_ADDR ||*/ address > END_ADDR)
				cout << "Error: breakpoint out of memory range or not a number" << endl;
			else {
				breakpoints.push_back(address);
				sort(breakpoints.begin(), breakpoints.end());
			}
		}
		else if (regex_match (cmd, regex(SYNTAX_BREAKIF_V) )) {
			uint16_t x;
			uint8_t val;

			//Extracting V register number
			regex rule(NREG);
			x = getNumber(cmd, rule);

			//Extracting V register condition value
			rule = REGVAL;
			val = getNumber(cmd, rule);

			Vbreakpoints[x] = val;
		}
		else if (regex_match (cmd, regex(SYNTAX_BREAKIF_R) )) {
			uint16_t x;
			uint8_t val;

			//Extracting V register number
			regex rule(NREG);
			x = getNumber(cmd, rule);

			//Extracting V register condition value
			rule = REGVAL;
			val = getNumber(cmd, rule);

			Rbreakpoints[x] = val;
		}
		else if (regex_match (cmd, regex(SYNTAX_BREAKIF_I) )) {
			uint16_t val;

			//Extracting V register condition value
			regex rule(REGVAL);
			val = getNumber(cmd, rule);

			if (val > (RAMSIZE - 1))
				cout << "Error: register I cannot point out of memory location" << endl;
			else
				Ibreakpoint = val;
		}
		else if (regex_match (cmd, regex(SYNTAX_BREAKIF_SP) )) {
			uint16_t val;

			//Extracting V register condition value
			regex rule(REGVAL);
			val = getNumber(cmd, rule);

			if (val > (RAMSIZE - 1))
				cout << "Error: register SP cannot point out of memory location" << endl;
			else
				SPbreakpoint = val;
		}
		else if (regex_match (cmd, regex(SYNTAX_BREAKIF_DT) )) {
			uint8_t val;

			//Extracting V register condition value
			regex rule(REGVAL);
			val = getNumber(cmd, rule);
			DTbreakpoint = val;
		}
		else if (regex_match (cmd, regex(SYNTAX_BREAKIF_ST) )) {
			uint8_t val;

			//Extracting V register condition value
			regex rule(REGVAL);
			val = getNumber(cmd, rule);
			STbreakpoint = val;
		}
		else if (regex_match (cmd, regex(SYNTAX_UNBREAK) )) {
			breakpoints.clear();
		}
		else if (regex_match (cmd, regex(SYNTAX_UNBREAKIF_V) )) {
			regex rule(REGVAL);
			uint16_t x = getNumber(cmd, rule);
			Vbreakpoints[x] = ND;
		}
		else if (regex_match (cmd, regex(SYNTAX_UNBREAKIF_R) )) {
			regex rule(REGVAL);
			uint16_t x = getNumber(cmd, rule);
			Rbreakpoints[x] = ND;
		}
		else if (regex_match (cmd, regex(SYNTAX_UNBREAKIF_I) )) {
			Ibreakpoint = ND;
		}
		else if (regex_match (cmd, regex(SYNTAX_UNBREAKIF_SP) )) {
			SPbreakpoint = ND;
		}
		else if (regex_match (cmd, regex(SYNTAX_UNBREAKIF_DT) )) {
			DTbreakpoint = ND;
		}
		else if (regex_match (cmd, regex(SYNTAX_UNBREAKIF_ST) )) {
			STbreakpoint = ND;
		}
		else if (regex_match (cmd, regex(SYNTAX_BREAKLIST) )) {
			printAddressBreakPoints();
		}
		else if (regex_match (cmd, regex(SYNTAX_BREAKIFLIST) )) {
			printConditionBreakPoints();
		}
		else if (regex_match (cmd, regex(SYNTAX_PRINT_M) )) {
			regex rule(M_ADDRESS);
			uint16_t position = 0;
			uint16_t startAddr = getNumber(cmd, rule, &position);
			uint16_t endAddr = getNumber(cmd.substr(position, cmd.size() - 1), rule);

			if (endAddr == 0)
				endAddr = startAddr;

			if (startAddr >= RAMSIZE || endAddr >= RAMSIZE)
				cout << "Error, out of memory address" << endl;
			else if (startAddr > endAddr)
				cout << "Syntax error, start address > end address" << endl;
			else
				myChip8->dumpMem(startAddr, endAddr);
		}
		else if (regex_match (cmd, regex(SYNTAX_PRINT_VM) )) {
			regex rule(M_ADDRESS);
			uint16_t position = 0;
			uint16_t startAddr = getNumber(cmd, rule, &position);
			uint16_t endAddr = getNumber(cmd.substr(position, cmd.size() - 1), rule);

			if (endAddr == 0)
				endAddr = startAddr;

			if (startAddr >= VRAMSIZE || endAddr >= VRAMSIZE)
				cout << "Error, out of memory address" << endl;
			else if (startAddr > endAddr)
				cout << "Syntax error, start address > end address" << endl;
			else
				myChip8->dumpVmem(startAddr, endAddr);
		}
		else if (regex_match (cmd, regex(SYNTAX_PRINT_VX) )) {
			regex rule(XREG);
			uint16_t x = getNumber(cmd, rule);

			cout << "0x" << uint16_t(myChip8->getV(x)) << endl;
		}
		else if (regex_match (cmd, regex(SYNTAX_PRINT_RX) )) {
			regex rule(XREG);
			uint16_t x = getNumber(cmd, rule);

			cout << "0x" << uint16_t(myChip8->getR(x)) << endl;
		}
		else if (regex_match (cmd, regex(SYNTAX_PRINT_I) )) {
			cout << "0x" << uint16_t(myChip8->getI()) << endl;
		}
		else if (regex_match (cmd, regex(SYNTAX_PRINT_DT) )) {
			cout << "0x" << uint16_t(myChip8->getDT()) << endl;
		}
		else if (regex_match (cmd, regex(SYNTAX_PRINT_ST) )) {
			cout << "0x" << uint16_t(myChip8->getST()) << endl;
		}
		else if (regex_match (cmd, regex(SYNTAX_PRINT_SP) )) {
			cout << "0x" << uint16_t(myChip8->getSP()) << endl;
		}
		else if (regex_match (cmd, regex(SYNTAX_SET_V) )) {
			uint16_t x;
			uint8_t val;

			//Extracting V register number
			regex rule(NREG);
			x = getNumber(cmd, rule);

			//Extracting V register new value
			rule = REGVAL;
			val = getNumber(cmd, rule);

			myChip8->setV(x, val);
		}
		else if (regex_match (cmd, regex(SYNTAX_SET_R) )) {
			uint16_t x;
			uint8_t val;

			//Extracting V register number
			regex rule(NREG);
			x = getNumber(cmd, rule);

			//Extracting V register new value
			rule = REGVAL;
			val = getNumber(cmd, rule);

			myChip8->setR(x, val);
		}
		else if (regex_match (cmd, regex(SYNTAX_SET_I) )) {
			uint16_t val;

			//Extracting I register new value
			regex rule(ADDRESSVAL);
			val = getNumber(cmd, rule);

			myChip8->setI(val);
		}
		else if (regex_match (cmd, regex(SYNTAX_SET_SP) )) {
			uint16_t val;

			//Extracting SP register new value
			regex rule(ADDRESSVAL);
			val = getNumber(cmd, rule);

			myChip8->setSP(val);
		}
		else if (regex_match (cmd, regex(SYNTAX_SET_DT) )) {
			uint8_t val;

			//Extracting DT register new value
			regex rule(REGVAL);
			val = getNumber(cmd, rule);

			myChip8->setDT(val);
		}
		else if (regex_match (cmd, regex(SYNTAX_SET_ST) )) {
			uint8_t val;

			//Extracting ST register new value
			regex rule(REGVAL);
			val = getNumber(cmd, rule);

			myChip8->setST(val);
		}
		else if (regex_match (cmd, regex(SYNTAX_POKE) )) {
			uint16_t nTokens = countTokens(cmd);
			regex rule(M_ADDRESS);
			uint16_t position = 0;
			uint16_t startAddr = getNumber(cmd, rule, &position);
			uint16_t endAddr = 0;

			//poke startAddr endAddr = value
			if (nTokens > 3)
				endAddr = getNumber(cmd.substr(position, cmd.size() - 1), rule, &position);
			//poke address = value
			else
				endAddr = startAddr;

			rule = REGVAL;
			uint8_t val = getNumber(cmd.substr(position, cmd.size() - 1), rule);

			if (startAddr >= RAMSIZE || endAddr >= RAMSIZE)
				cout << "Error, out of memory address" << endl;
			else if (startAddr > endAddr)
				cout << "Syntax error, start address > end address" << endl;
			else
				myChip8->poke(startAddr, endAddr, val);
		}
		else if (regex_match (cmd, regex(SYNTAX_VPOKE) )) {
			uint16_t nTokens = countTokens(cmd);
			regex rule(M_ADDRESS);
			uint16_t position = 0;
			uint16_t startAddr = getNumber(cmd, rule, &position);
			uint16_t endAddr = 0;

			//vpoke startAddr endAddr = value
			if (nTokens > 3)
				endAddr = getNumber(cmd.substr(position, cmd.size() - 1), rule, &position);
			//vpoke address = value
			else
				endAddr = startAddr;

			rule = REGVAL;
			uint8_t val = getNumber(cmd.substr(position, cmd.size() - 1), rule);

			if (startAddr >= VRAMSIZE || endAddr >= VRAMSIZE)
				cout << "Error, out of memory address" << endl;
			else if (startAddr > endAddr)
				cout << "Syntax error, start address > end address" << endl;
			else {
				myChip8->vpoke(startAddr, endAddr, val);
				//Activate drawFlag and update screen to view immediately the result
				gui->drawFlag = true;
				gui->updateScreen(myChip8->getGfx());
			}
		}
		else if (regex_match (cmd, regex(SYNTAX_SETPIXEL) )) {
			regex rule(COORDINATE);
			uint16_t position = 0;
			uint8_t x = getNumber(cmd, rule, &position);
			uint8_t y = getNumber(cmd.substr(position, cmd.size() - 1), rule);
			uint8_t gfxWidth = myChip8->getGfxWidth();
			uint8_t gfxHeight = myChip8->getGfxHeight();

			if (x >= gfxWidth || y >= gfxHeight)
				cout << "Error, coordinates out of screen resolution" << endl;
			else {
				uint16_t nPixels = (x + y * gfxWidth);
				uint16_t address = nPixels / 8;
				uint8_t offset = nPixels % 8;
				uint8_t byte = myChip8->vpeek(address);
				uint8_t mask = (0x80 >> offset);
				if (byte & mask)
					myChip8->setV(0xf, 1); //pixel already set so do a collision!
				byte ^= mask;
				myChip8->vpoke(address, address, byte);
				//Activate drawFlag and update screen to view immediately the result
				gui->drawFlag = true;
				gui->updateScreen(myChip8->getGfx());
			}
		}
		else if (regex_match (cmd, regex(SYNTAX_CLEARPIXEL) )) {
			regex rule(COORDINATE);
			uint16_t position = 0;
			uint8_t x = getNumber(cmd, rule, &position);
			uint8_t y = getNumber(cmd.substr(position, cmd.size() - 1), rule);
			uint8_t gfxWidth = myChip8->getGfxWidth();
			uint8_t gfxHeight = myChip8->getGfxHeight();

			if (x >= gfxWidth || y >= gfxHeight)
				cout << "Error, coordinates out of screen resolution" << endl;
			else {
				uint16_t nPixels = (x + y * gfxWidth);
				uint16_t address = nPixels / 8;
				uint8_t offset = nPixels % 8;
				uint8_t byte = myChip8->vpeek(address);
				uint8_t mask = ~(0x80 >> offset);
				byte &= mask;
				myChip8->vpoke(address, address, byte);
				//Activate drawFlag and update screen to view immediately the result
				gui->drawFlag = true;
				gui->updateScreen(myChip8->getGfx());
			}
		}
		else if (regex_match (cmd, regex(SYNTAX_HELP) )) {
			regex rule(PARAM);
			string param = getParam(cmd, rule);
			//If we get help as string then no parameter has been specified
			if (param == "help")
				param = "";
			printHelp(param);
		}
		else if (regex_match (cmd, regex(SYNTAX_DISASM) )) {
			cout << myChip8->getDisasm();
		}
		else if (regex_match (cmd, regex(SYNTAX_QUIT) )) {
			resultCmd = QUIT;
			break;
		}
		else if (regex_match (cmd, regex(SYNTAX_PRINTCPU) )) {
			myChip8->dumpCpuStatus();
		}
		else
			cout << "Command not found or syntax error" << endl;
	}
	return resultCmd;
}
