#include <c8pp.h>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <random>

using namespace std;

//Chip8 fontset (at FONTSET_ADDR location) for low resolution
uint8_t Chip8::chip8_fontset[FONTSET_SIZE] =
{
		//Chip8 fonts
		0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
		0x20, 0x60, 0x20, 0x20, 0x70, // 1
		0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
		0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
		0x90, 0x90, 0xF0, 0x10, 0x10, // 4
		0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
		0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
		0xF0, 0x10, 0x20, 0x40, 0x40, // 7
		0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
		0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
		0xF0, 0x90, 0xF0, 0x90, 0x90, // A
		0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
		0xF0, 0x80, 0x80, 0x80, 0xF0, // C
		0xE0, 0x90, 0x90, 0x90, 0xE0, // D
		0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
		0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

uint8_t Chip8::schip48_fontset[HI_FONTSET_SIZE] =
{
		//S-Chip48 fonts
		0xF0, 0xF0, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0xF0, 0xF0, // 0
		0x20, 0x20, 0x60, 0x60, 0x20, 0x20, 0x20, 0x20, 0x70, 0x70, // 1
		0xF0, 0xF0, 0x10, 0x10, 0xF0, 0xF0, 0x80, 0x80, 0xF0, 0xF0, // 2
		0xF0, 0xF0, 0x10, 0x10, 0xF0, 0xF0, 0x10, 0x10, 0xF0, 0xF0, // 3
		0x90, 0x90, 0x90, 0x90, 0xF0, 0xF0, 0x10, 0x10, 0x10, 0x10, // 4
		0xF0, 0xF0, 0x80, 0x80, 0xF0, 0xF0, 0x10, 0x10, 0xF0, 0xF0, // 5
		0xF0, 0xF0, 0x80, 0x80, 0xF0, 0xF0, 0x90, 0x90, 0xF0, 0xF0, // 6
		0xF0, 0xF0, 0x10, 0x10, 0x20, 0x20, 0x40, 0x40, 0x40, 0x40, // 7
		0xF0, 0xF0, 0x90, 0x90, 0xF0, 0xF0, 0x90, 0x90, 0xF0, 0xF0, // 8
		0xF0, 0xF0, 0x90, 0x90, 0xF0, 0xF0, 0x10, 0x10, 0xF0, 0xF0, // 9
		0xF0, 0xF0, 0x90, 0x90, 0xF0, 0xF0, 0x90, 0x90, 0x90, 0x90, // A
		0xE0, 0xE0, 0x90, 0x90, 0xE0, 0xE0, 0x90, 0x90, 0xE0, 0xE0, // B
		0xF0, 0xF0, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0xF0, 0xF0, // C
		0xE0, 0xE0, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0xE0, 0xE0, // D
		0xF0, 0xF0, 0x80, 0x80, 0xF0, 0xF0, 0x80, 0x80, 0xF0, 0xF0, // E
		0xF0, 0xF0, 0x80, 0x80, 0xF0, 0xF0, 0x80, 0x80, 0x80, 0x80  // F
};

Chip8::Chip8(Video *gui) {
	this->gui = gui;
	//Initialize tables to nullptr
	memset(chip8Table, 		uint64_t(nullptr), CHIP8_TABLE_SIZE * sizeof(ptrMethodChip8));
	//Group tables
	memset(chip8grp8, 		uint64_t(nullptr), CHIP8_GRP8_SIZE * sizeof(ptrMethodChip8));
	memset(chip8grp0,		uint64_t(nullptr), CHIP8_GRP0_SIZE * sizeof(ptrMethodChip8));
	memset(chip8grp0grpE, 	uint64_t(nullptr), CHIP8_GRP0_GRPE_SIZE * sizeof(ptrMethodChip8));
	memset(chip8grp0grpF, 	uint64_t(nullptr), CHIP8_GRP0_GRPF_SIZE * sizeof(ptrMethodChip8));
	memset(chip8grpF, 		uint64_t(nullptr), CHIP8_GRPF_SIZE * sizeof(ptrMethodChip8));
	memset(chip8grpFgrp0, 	uint64_t(nullptr), CHIP8_GRPF_GRP0_SIZE * sizeof(ptrMethodChip8));
	memset(chip8grpFgrp1, 	uint64_t(nullptr), CHIP8_GRPF_GRP1_SIZE * sizeof(ptrMethodChip8));

	//Main jump table
	//nnnn
	chip8Table[0x0] = 			&Chip8::grp0;				//(0)nnn
	chip8Table[0x1] = 			&Chip8::jp;					//(1)nnn
	chip8Table[0x2] = 			&Chip8::call;				//(2)nnn
	chip8Table[0x3] = 			&Chip8::seVxByte;			//(3)xnn
	chip8Table[0x4] = 			&Chip8::sneVxByte;			//(4)xnn
	chip8Table[0x5] = 			&Chip8::seVxVy;				//(5)xyk
	chip8Table[0x6] = 			&Chip8::ldVxByte;			//(6)xnn
	chip8Table[0x7] = 			&Chip8::addVxByte;			//(7)xkk
	chip8Table[0x8] = 			&Chip8::grp8;				//(8)nnn
	chip8Table[0x9] = 			&Chip8::sneVxVy;			//(9)xyk
	chip8Table[0xA] = 			&Chip8::ldIAddr;			//(A)nnn
	chip8Table[0xB] = 			&Chip8::jpV0Addr;			//(B)nnn
	chip8Table[0xC] = 			&Chip8::rndVxByte;			//(C)xnn
	chip8Table[0xD] = 			&Chip8::drwVxVyNibble;		//(D)xyn
	chip8Table[0xE] = 			&Chip8::skip;				//(E)xkk
	chip8Table[0xF] = 			&Chip8::grpF;				//(F)nnn

	//8xyn
	chip8grp8[0x0] = 			&Chip8::ldVxVy;				//8xy(0)
	chip8grp8[0x1] =			&Chip8::orVxVy;				//8xy(1)
	chip8grp8[0x2] =			&Chip8::andVxVy;			//8xy(2)
	chip8grp8[0x3] =			&Chip8::xorVxVy;			//8xy(3)
	chip8grp8[0x4] =			&Chip8::addVxVy;			//8xy(4)
	chip8grp8[0x5] =			&Chip8::subVxVy;			//8xy(5)
	chip8grp8[0x6] =			&Chip8::shrVx;				//8xy(6)
	chip8grp8[0x7] =			&Chip8::subnVxVy;			//8xy(7)
	chip8grp8[0xE] =			&Chip8::shlVx;				//8xy(E)

	//00nx
	chip8grp0[0x0] = 			&Chip8::cpuNull; 			//00(0)0
	chip8grp0[0xC] = 			&Chip8::scdn; 				//00(C)x
	chip8grp0[0xE] = 			&Chip8::grp0grpE; 			//00(E)x
	chip8grp0[0xF] = 			&Chip8::grp0grpF; 			//00(F)x

	//00Fx
	chip8grp0grpF[0xB] =		&Chip8::scr;				//00F(B)
	chip8grp0grpF[0xC] =		&Chip8::scl;				//00F(C)
	chip8grp0grpF[0xD] =		&Chip8::exit;				//00F(D)
	chip8grp0grpF[0xE] =		&Chip8::low;				//00F(E)
	chip8grp0grpF[0xF] =		&Chip8::high;				//00F(F)

	//00Ex
	chip8grp0grpE[0x0] = 		&Chip8::cls; 				//00E(0)
	chip8grp0grpE[0xE] = 		&Chip8::ret; 				//00E(E)

	//Fxxn
	chip8grpF[0x0] = 			&Chip8::grpFgrp0; 			//Fx(0)k
	chip8grpF[0x1] = 			&Chip8::grpFgrp1; 			//Fx(1)k
	chip8grpF[0x2] = 			&Chip8::ldFVx; 				//Fx(2)9
	chip8grpF[0x3] = 			&Chip8::grpFgrp3; 			//Fx(3)k
	chip8grpF[0x5] = 			&Chip8::ldDeRefIVx; 		//Fx(5)5
	chip8grpF[0x6] = 			&Chip8::ldVxDeRefI; 		//Fx(6)5
	chip8grpF[0x7] = 			&Chip8::ldRVx;				//Fx(7)5
	chip8grpF[0x8] = 			&Chip8::ldVxR; 				//Fx(8)5

	//Fx3x
	chip8grpFgrp3[0x0] =		&Chip8::ldHfVx;				//Fx3(0)
	chip8grpFgrp3[0x3] =		&Chip8::ldBVx;				//Fx3(3)

	//Fx0x
	chip8grpFgrp0[0x7] = 		&Chip8::ldVxDt; 			//Fx0(7)
	chip8grpFgrp0[0xA] = 		&Chip8::ldVxK; 				//Fx0(A)

	//Fx1x
	chip8grpFgrp1[0x5] = 		&Chip8::ldDtVx; 			//Fx1(5)
	chip8grpFgrp1[0x8] = 		&Chip8::ldStVx; 			//Fx1(8)
	chip8grpFgrp1[0xE] = 		&Chip8::addIVx; 			//Fx1(E)

	//Quirk default values
	shiftQuirk = false;
	loadStoreQuirk = false;
	packedBcdQuirk = false;
	imageWrapQuirk = false;
	biosMode = false;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
	#endif
}

//*************** Opcodes Group selectors **********************
void Chip8::grp8() {
	#ifdef DEBUG_SHOW_GRPSEL
		cout << "GROUP 8 selected" << endl;
	#endif
	//We get last 4 bits for arithmetic instruction
	ptrMethodChip8 execute = Chip8::chip8grp8[(opcode & 0x000f)];
	if (execute)
		(this->*execute)();
	else
		exception = EXC_UNKNOWN_OPCODE;
}

void Chip8::grp0() {
	#ifdef DEBUG_SHOW_GRPSEL
		cout << "GROUP 0 selected" << endl;
	#endif
	//We get last 8 bits for general instruction
	ptrMethodChip8 execute = Chip8::chip8grp0[(opcode & 0x00f0) >> 4];
	if (execute)
		(this->*execute)();
	else
		exception = EXC_UNKNOWN_OPCODE;
}

void Chip8::grp0grpE() {
	#ifdef DEBUG_SHOW_GRPSEL
		cout << "GROUP 0, GROUP E selected" << endl;
	#endif
	//We get last 8 bits for general instruction
	ptrMethodChip8 execute = Chip8::chip8grp0grpE[(opcode & 0x000f)];
	if (execute)
		(this->*execute)();
	else
		exception = EXC_UNKNOWN_OPCODE;
}

void Chip8::grp0grpF() {
	#ifdef DEBUG_SHOW_GRPSEL
		cout << "GROUP 0, GROUP F selected" << endl;
	#endif
	//We get last 8 bits for general instruction
	ptrMethodChip8 execute = Chip8::chip8grp0grpF[(opcode & 0x000f)];
	if (execute)
		(this->*execute)();
	else
		exception = EXC_UNKNOWN_OPCODE;
}

void Chip8::grpF() {
	#ifdef DEBUG_SHOW_GRPSEL
		cout << "GROUP F selected" << endl;
	#endif
	//We get 4 bits for F group instructions
	ptrMethodChip8 execute = Chip8::chip8grpF[(opcode & 0x00f0) >> 4];
	if (execute)
		(this->*execute)();
	else
		exception = EXC_UNKNOWN_OPCODE;
}

void Chip8::grpFgrp0() {
	#ifdef DEBUG_SHOW_GRPSEL
		cout << "GROUP F, GROUP 0 selected" << endl;
	#endif
	//We get last 4 bits for F0 group instructions
	ptrMethodChip8 execute = Chip8::chip8grpFgrp0[(opcode & 0x000f)];
	if (execute)
		(this->*execute)();
	else
		exception = EXC_UNKNOWN_OPCODE;
}

void Chip8::grpFgrp1() {
	#ifdef DEBUG_SHOW_GRPSEL
		cout << "GROUP F, GROUP 1 selected" << endl;
	#endif
	//We get last 4 bits for F1 group instructions
	ptrMethodChip8 execute = Chip8::chip8grpFgrp1[(opcode & 0x000f)];
	if (execute)
		(this->*execute)();
	else
		exception = EXC_UNKNOWN_OPCODE;
}

void Chip8::grpFgrp3() {
	#ifdef DEBUG_SHOW_GRPSEL
		cout << "GROUP F, GROUP 3 selected" << endl;
	#endif
	//We get last 4 bits for F3 group instructions
	ptrMethodChip8 execute = Chip8::chip8grpFgrp3[(opcode & 0x000f)];
	if (execute)
		(this->*execute)();
	else
		exception = EXC_UNKNOWN_OPCODE;
}

//*********************** Instructions *********************

//NOP
void Chip8::cpuNull() {
	//Select only code 0000 not 000x
	if (opcode == 0x0000) {
		//Do nothing
		pc += 2;
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "NOP" << endl;
		#endif
	}
	else
		exception = EXC_UNKNOWN_OPCODE;
}

//SCD nibble (00Cn)
void Chip8::scdn() {
	if (opcode >= 0x00C0 && opcode <= 0x00CF) {
		uint8_t n = (opcode & 0x000f);

		if (gui->getScreenMode() == NORMAL)
			n /= 2;

		if (delayTimer == 0) {
			uint16_t lastIndex = ((gui->gfxWidth * gui->gfxHeight) / 8) - 1;
			//Total Bytes to cut off
			uint16_t cutOffset = (gui->gfxWidth * n) / 8;
			#ifdef DEBUG_SCROLLDOWN
				cout << "LastIndex: " << uint16_t(lastIndex) << endl;
				cout << "cutOffset: " << uint16_t(cutOffset) << endl;
				cout << "n: " << uint16_t(n) << endl;
			#endif
			//Shifts to the beginning all VRAM, from cutOffset till end
			//SCROLLUP
			/*for (uint16_t i = cutOffset ; i <= lastIndex ; ++i) {
				uint16_t j = i - cutOffset;
				gfx[j] = gfx[i];
				gfx[i] = 0;
			}*/
			//SCROLLDOWN
			for (int16_t i = (lastIndex - cutOffset) ; i >= 0 ; --i) {
				uint16_t j = i + cutOffset;
				gfx[j] = gfx[i];
				gfx[i] = 0;
			}
			pc += 2;
			gui->drawFlag = true;
		}
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "SCD 0x" << hex << uint16_t(n) << endl;
		#endif
	}
	else
		exception = EXC_UNKNOWN_OPCODE;
}

//SCR (00FC)
void Chip8::scr() {
	uint8_t n = 4;

	if (gui->getScreenMode() == NORMAL)
		n = 2;

	if (delayTimer == 0) {
		uint16_t rows = gui->gfxHeight;
		uint16_t cols = gui->gfxWidth / 8;
		//Scroll 4 pixels to right
		for (uint16_t i = 0 ; i < rows ; ++i) {
			for (int16_t j = cols - 1 ; j >= 0 ; --j) {
				uint16_t index = j + i * cols;
				if (n == 4) {
					if (j == 0)
						gfx[index] = (gfx[index] >> n) & 0x0f;
					else
						gfx[index] = (gfx[index] >> n) | ((gfx[index - 1] & 0x0f) << n);
				}
				else if (n == 2) {
					if (j == 0)
						gfx[index] = (gfx[index] >> n) & 0x3f;
					else
						gfx[index] = (gfx[index] >> n) | ((gfx[index - 1] & 0x03) << (8 - n));
				}
			}
		}
		pc += 2;
		gui->drawFlag = true;
	}
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "SCR" << endl;
	#endif
}

//SCL (00FB)
void Chip8::scl() {
	uint8_t n = 4;
	if (gui->getScreenMode() == NORMAL)
		n = 2;

	if (delayTimer == 0) {
		uint16_t rows = gui->gfxHeight;
		uint16_t cols = gui->gfxWidth / 8;
		//Scroll 4 pixels to right
		for (uint16_t i = 0 ; i < rows ; ++i) {
			for (uint16_t j = 0 ; j < cols ; ++j) {
				uint16_t index = j + i * cols;
				if (n == 4) {
					if (j < cols - 1)
						gfx[index] = (gfx[index] << n) | ((gfx[index + 1] & 0xf0) >> n);
					else
						gfx[index] = (gfx[index] << n) & 0xf0;
				}
				else if (n == 2) {
					if (j < cols - 1)
						gfx[index] = (gfx[index] << n) | ((gfx[index + 1] & 0xc0) >> (8 - n));
					else
						gfx[index] = (gfx[index] << n) & 0xfc;
				}
			}
		}
		pc += 2;
		gui->drawFlag = true;
	}
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "SCL" << endl;
	#endif
}

//EXIT (00FD)
void Chip8::exit() {
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "EXIT" << endl;
	#endif
	gui->quit();
	exception = EXC_NO_EXCEPTION;
}

//LOW (00FE)
void Chip8::low() {
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "LOW" << endl;
	#endif
	gui->setResolution(DEFAULT_GFX_WIDTH, DEFAULT_GFX_HEIGHT);
	gui->setScreenMode(NORMAL);
	pc += 2;
}

//HIGH (00FF)
void Chip8::high() {
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "HIGH" << endl;
	#endif
	gui->setResolution(HIGH_GFX_WIDTH, HIGH_GFX_HEIGHT);
	gui->setScreenMode(HIGH);
	pc += 2;
}

//CLS (00E0)
void Chip8::cls() {
	if (opcode == 0x00E0) {
		memset(gfx, 0, (gui->gfxWidth * gui->gfxHeight) / 8);
		gui->drawFlag = true;
		pc += 2;
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "CLS" << endl;
		#endif
	}
	else
		exception = EXC_UNKNOWN_OPCODE;
}

//RET (00EE)
void Chip8::ret() {
	if (opcode == 0x00EE) {
		pc = (uint16_t(memory[sp]) << 8) | (uint16_t(memory[sp + 1]));
		sp -= 2;
		if (sp < (SP_BASE_ADDR - 2))
			exception = EXC_EMPTY_STACK; //throws empty stack exception
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "RET" << endl;
		#endif
	}
	else
		exception = EXC_UNKNOWN_OPCODE;
}

//SE Vx, byte (3xkk)
void Chip8::seVxByte() {
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t kk = (opcode & 0x00ff);

	//Skip next instruction if Vx == kk
	if (V[x] == kk)
		pc += 2;
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "SE V" << hex << uint16_t(x) << ", 0x" << hex << uint16_t(kk) << endl;
	#endif
}

//SNE Vx, byte (4xkk)
void Chip8::sneVxByte() {
	uint16_t x = (opcode & 0x0f00) >> 8;
	uint16_t kk = (opcode & 0x00ff);

	//Skip next instruction if Vx != kk
	if (V[x] != uint8_t(kk))
		pc += 2;
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "SNE V" << hex << x << ", 0x" << hex << kk << endl;
	#endif
}

//SNE Vx, Vy (9xy0)
void Chip8::sneVxVy() {
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t y = (opcode & 0x00f0) >> 4;

	//Skip next instruction if Vx != kk
	if (V[x] != V[y])
		pc += 2;
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "SNE V" << hex << uint16_t(x) << ", V" << hex << uint16_t(y) << endl;
	#endif
}

//SE Vx, Vy (5xkk)
void Chip8::seVxVy() {
	uint16_t x = (opcode & 0x0f00) >> 8;
	uint16_t y = (opcode & 0x00f0) >> 4;

	//Skip next instruction if Vx == Vy
	if (V[x] == V[y])
		pc += 2;
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "SE V" << hex << x << ", V" << hex << y << endl;
	#endif
}

//CALL addr (2nnn)
void Chip8::call() {
	uint16_t addr = (opcode & 0x0fff);
	if (/*addr < START_ADDR ||*/ addr > END_ADDR)
		exception = EXC_OUT_OF_MEMORY;
	else {
		sp += 2;
		memory[sp] = ((pc + 2) & 0xff00) >> 8;
		memory[sp + 1] = ((pc + 2) & 0x00ff);
		pc = addr;
		if (sp > SP_MAX_ADDR)
			exception = EXC_FULL_STACK; //throws full stack exception
	}
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "CALL 0x" << hex << addr << endl;
	#endif
}

//JP addr (1nnn)
void Chip8::jp() {
	uint16_t addr = (opcode & 0x0fff);
	if (/*addr < START_ADDR ||*/ addr > END_ADDR)
		exception = EXC_OUT_OF_MEMORY;
	else
		pc = addr;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "JP 0x" << hex << addr << endl;
	#endif
}

//ADD Vx, byte
void Chip8::addVxByte() {
	//Fetching x from opcode
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t byte = (opcode & 0x00ff);
	V[x] = V[x] + byte;
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "ADD V" << hex << uint16_t(x) << ", 0x" << hex << uint16_t(byte) << endl;
	#endif
}

//LD I, addr (Annn)
void Chip8::ldIAddr() {
	uint16_t addr = (opcode & 0x0fff);
	I = addr;
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "LD I, 0x" << hex << addr << endl;
	#endif
}

//JP V0, addr (Bnnn)
void Chip8::jpV0Addr() {
	uint16_t addr = (opcode & 0x0fff);
	if (/*(addr + V[0]) < START_ADDR ||*/ (addr + V[0]) > END_ADDR)
		exception = EXC_OUT_OF_MEMORY;
	else
		pc = addr + V[0];
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "JP V0, 0x" << hex << addr << endl;
	#endif
}

//RND Vx, byte (Cxkk)
void Chip8::rndVxByte() {
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t kk = (opcode & 0x00ff);

	//Generating seed and random number
	random_device rd;
    mt19937 mt(rd());
    uniform_int_distribution<uint8_t> distribution(0, 255);
    uint8_t number = distribution(mt);

	#ifdef DEBUG_RND
		cout << "Rnd number: " << uint16_t(number) << endl;
	#endif

	V[x] = number & kk;
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "RND V" << hex << uint16_t(x) << ", 0x" << hex << uint16_t(kk) << endl;
	#endif
}

//DRW Vx, Vy, nibble (Dxyn) and DRW Vx, Vy, 0 (Dxy0)
void Chip8::drwVxVyNibble() {
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t y = (opcode & 0x00f0) >> 4;
	uint8_t col = V[x];
	uint8_t row = V[y];
	uint8_t n = (opcode & 0x000f);
	//Coordinates are V[x] and V[y]
	//In memory[I] ... memory[I + n] we have the sprite pixel data
	//Video output is 64 x 32 pixels gfxWidth and gfxHeight
	V[0xF] = 0;
	#ifdef DEBUG_GFX
		cout << "DEBUG drwVxVyNibble()" << endl;
	#endif
	//We set the area to update for texture
	//gui->setUpdateArea(row, col, 8, n);
	//Wrapping to the opposite side of screen if out of bound

	if (n > 0) {
		for (uint8_t yline = 0 ; yline < n ; ++yline)
		{
			//8 pixels per sprite row
			for (uint8_t xline = 0 ; xline < 8 ; ++xline)
			{
				//We get single pixel value in ram
				//pixelRam & 10000000, pixelRam & 01000000, ... , pixelRam & 00000001
				uint8_t pixelRam = memory[I + yline] & (0x80 >> xline);
				uint16_t totalBits;

				//Handle image wrapping or not
				if (imageWrapQuirk)
					totalBits = ((col + xline) % gui->gfxWidth) +
									(((row + yline) % gui->gfxHeight) * gui->gfxWidth);
				else
					totalBits = (col + xline) + ((row + yline) * gui->gfxWidth);

				//VRAM format, 1 bit per pixel (same for RAM)
				uint16_t index = totalBits / 8;
				uint8_t offset = totalBits % 8;
				uint8_t pixelGfx = (gfx[index] & (0x80 >> offset));
				#ifdef DEBUG_GFX
					cout << "pixelRam: " << dec << uint16_t(pixelRam) << endl;
					cout << "pixelGfx: " << dec << uint16_t(pixelRam) << endl;
					cout << "col: " << dec << uint16_t(col) << endl;
					cout << "xline: " << dec << uint16_t(xline) << endl;
					cout << "row: " << dec << uint16_t(row) << endl;
					cout << "yline: " << dec << uint16_t(yline) << endl;
					cout << "Gfx(col, row): 0x" << hex << uint16_t(pixelGfx) << endl;
				#endif

				if (pixelRam != 0) {
					if (pixelGfx != 0)
						V[0xF] = 1; //collision!
					//We write the pixel to video ram
					gfx[index] ^= (0x80 >> offset);
				}
			}
		}
	}
	else { //extended draw 16x16 sprites
		uint16_t i = I;
		uint8_t nextLimit = 2;

		//If low resolution then sprites are 8x16 rather than 16x16
		if (gui->getScreenMode() == NORMAL)
			nextLimit = 1;

		for (uint8_t yline = 0 ; yline < 16 ; ++yline)
		{
			uint8_t pixelRam;
			uint16_t totalBits;
			uint16_t index;
			uint8_t pixelGfx;
			uint8_t offset;
			//We need to read 16 pixels per sprite row
			for (uint8_t next = 0 ; next < nextLimit ; ++next)
			{
				for (uint8_t xline = 0 ; xline < 8 ; ++xline)
				{
					//We get single pixel value in ram
					//pixelRam & 10000000, pixelRam & 01000000, ... , pixelRam & 00000001

					//When 16x16 each yline is 2 bytes so we need to multiply it by 2 (nextLimit = 2)
					//When 8x16 each yline is 1 byte so no multiply (nextLimit = 1)
					pixelRam = memory[i + yline * nextLimit + next] & (0x80 >> xline);
					if (imageWrapQuirk)
						totalBits = ((col + xline) % gui->gfxWidth) +
										(((row + yline) % gui->gfxHeight) * gui->gfxWidth);
					else
						totalBits = ((col + xline)) + (((row + yline)) * gui->gfxWidth);
					//VRAM format, 1 bit per pixel (same for RAM)
					index = totalBits / 8;
					offset = (totalBits % 8);
					pixelGfx = gfx[index + next] & (0x80 >> offset);

					if (pixelRam != 0) {
						if (pixelGfx != 0)
							V[0xF] = 1; //collision!
						//We write the pixel to video ram
						gfx[index + next] ^= (0x80 >> offset);
					}
				}
			}
		}
	}

	gui->drawFlag = true;
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "DRW V" << hex << uint16_t(x) << ", V" << hex << uint16_t(y) << ", 0x" << hex << uint16_t(n) << endl;
	#endif
}

//LD Vx, byte
void Chip8::ldVxByte() {
	//Fetching x from opcode
	uint16_t x = (opcode & 0x0f00) >> 8;
	uint8_t byte = (opcode & 0x00ff);
	V[x] = byte;
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "LD V" << hex << x << ", 0x" << hex << uint16_t(byte) << endl;
	#endif
}

//LD Vx, Vy
void Chip8::ldVxVy() {
	//Fetching x and y parameters from opcode
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t y = (opcode & 0x00f0) >> 4;
	V[x] = V[y];
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "LD V" << hex << uint16_t(x) << ", V" << hex << uint16_t(y) << endl;
	#endif
}

//OR Vx, Vy
void Chip8::orVxVy() {
	//Fetching x and y parameters from opcode
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t y = (opcode & 0x00f0) >> 4;
	V[x] = V[x] | V[y];
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "OR V" << hex << uint16_t(x) << ", V" << hex << uint16_t(y) << endl;
	#endif
}

//AND Vx, Vy
void Chip8::andVxVy() {
	//Fetching x and y parameters from opcode
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t y = (opcode & 0x00f0) >> 4;
	V[x] = V[x] & V[y];
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "AND V" << hex << uint16_t(x) << ", V" << hex << uint16_t(y) << endl;
	#endif
}

//XOR Vx, Vy
void Chip8::xorVxVy() {
	//Fetching x and y parameters from opcode
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t y = (opcode & 0x00f0) >> 4;
	V[x] = V[x] ^ V[y];
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "XOR V" << hex << uint16_t(x) << ", V" << hex << uint16_t(y) << endl;
	#endif
}

//ADD Vx, Vy
void Chip8::addVxVy() {
	//Fetching x and y parameters from opcode
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t y = (opcode & 0x00f0) >> 4;

	if (V[y] > (0xff - V[x]))
		V[0xF] = 1;
	else
		V[0xF] = 0;
	V[x] = V[x] + V[y];
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "ADD V" << hex << uint16_t(x) << ", V" << hex << uint16_t(y) << endl;
	#endif
}

//SUB Vx, Vy
void Chip8::subVxVy() {
	//Fetching x and y parameters from opcode
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t y = (opcode & 0x00f0) >> 4;

	if (V[x] >= V[y])
		V[0xF] = 1;
	else
		V[0xF] = 0;

	V[x] = V[x] - V[y];
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "SUB V" << hex << uint16_t(x) << ", V" << hex << uint16_t(y) << endl;
	#endif
}

//SHR Vx, {Vy}
void Chip8::shrVx() {
	//Fetching x and y parameters from opcode
	uint8_t x = (opcode & 0x0f00) >> 8;
	if (shiftQuirk) {
		//Ignore y parameter, shift Vx register (SHIFT_QUIRK enabled)
		if (V[x] & 0x01)
			V[0xF] = 1;
		else
			V[0xF] = 0;
		V[x] >>= 1;
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "SHR V" << hex << uint16_t(x) << endl;
		#endif
	}
	else {
		//Shift Vy register and store the result to Vx (SHIFT_QUIRK disabled)
		uint8_t y = (opcode & 0x00f0) >> 4;
		if (V[y] & 0x01)
			V[0xF] = 1;
		else
			V[0xF] = 0;
		V[y] >>= 1;
		V[x] = V[y];
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "SHR V" << hex << uint16_t(x) << ", V" << hex << uint16_t(y) << endl;
		#endif
	}

	pc += 2;
}

//SUBN Vx, Vy
void Chip8::subnVxVy() {
	//Fetching x and y parameters from opcode
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t y = (opcode & 0x00f0) >> 4;

	if (V[y] >= V[x])
		V[0xF] = 1;
	else
		V[0xF] = 0;

	V[x] = V[y] - V[x];
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "SUBN V" << hex << uint16_t(x) << ", V" << hex << uint16_t(y) << endl;
	#endif
}

//SHL Vx, {Vy}
void Chip8::shlVx() {
	//Fetching x and y parameters from opcode
	uint8_t x = (opcode & 0x0f00) >> 8;
	if (shiftQuirk) {
		//Ignore y parameter, shift Vx register (SHIFT_QUIRK enabled)
		if (V[x] & 0x80)
			V[0xF] = 1;
		else
			V[0xF] = 0;
		V[x] <<= 1;
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "SHL V" << hex << uint16_t(x) << endl;
		#endif
	}
	else {
		//Shift Vy register and store the result to Vx (SHIFT_QUIRK disabled)
		uint8_t y = (opcode & 0x00f0) >> 4;
		if (V[y] & 0x80)
			V[0xF] = 1;
		else
			V[0xF] = 0;
		V[y] <<= 1;
		V[x] = V[y];
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "SHL V" << hex << uint16_t(x) << ", V" << hex << uint16_t(y) << endl;
		#endif
	}

	pc += 2;
}

//We just do an if for two only values for this group (9E, A1)
//SKP Vx
//SKNP Vx
void Chip8::skip() {
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t skipCode = (opcode & 0x00ff);

	if (skipCode == 0x9e) {//Ex9E
		if (gui->keyStatus[V[x]])
			pc += 2; //do skip
		pc += 2;
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "SKP V" << hex << uint16_t(x) << endl;
		#endif
	}
	else if (skipCode == 0xa1) {//ExA1
		if (!gui->keyStatus[V[x]])
			pc += 2;
		pc += 2;
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "SKNP V" << hex << uint16_t(x) << endl;
		#endif
	}
	else
		exception = EXC_UNKNOWN_OPCODE;
}

//LD F, Vx
void Chip8::ldFVx() {
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t fCode = (opcode & 0x000f);
	//Fx2(9)
	if (fCode == 0x9) {

		I = GETFONT_ADDR((V[x] & 0x0f));
		pc += 2;
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "LD F, V" << hex << uint16_t(x) << endl;
		#endif
	}
	else
		exception = EXC_UNKNOWN_OPCODE;
}

//LD B, Vx
void Chip8::ldBVx() {
	uint8_t x = (opcode & 0x0f00) >> 8;
	//Fx3(3)
	if (packedBcdQuirk) {
		//Chip8 BCD is packed, so 4 bit for every digit, digit -> nibble
		memory[I] = (((V[x] / 10) % 10) << 4) | (V[x] / 100);
		memory[I + 1] = (V[x] % 100) % 10;
	}
	else {
		//Chip8 BCD is unpacked, so 8 bit for every digit, digit -> byte
		memory[I] = V[x] / 100;
		memory[I + 1] = (V[x] / 10) % 10;
		memory[I + 2] = (V[x] % 100) % 10;
	}
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "LD B, V" << hex << uint16_t(x) << endl;
	#endif
}

//LD HF, Vx
void Chip8::ldHfVx() {
	uint8_t x = (opcode & 0x0f00) >> 8;
	//Fx3(0)
	if (V[x] > 0xf)
		V[x] &= 0x0f;

	I = GETHI_FONT_ADDR(V[x]);

	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "LD HF, V" << hex << uint16_t(x) << endl;
	#endif
}

//LD [I], Vx
void Chip8::ldDeRefIVx() {
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t fCode = (opcode & 0x000f);
	//Fx5(5)
	if (fCode == 0x5) {
		uint16_t i = I;
		for (uint8_t reg = 0 ; reg <= x ; ++reg, ++i)
			memory[i] = V[reg];
		//I register incremented (LOAD_STORE_QUIRK enabled)
		if (loadStoreQuirk)
			I += (x + 1);
		pc += 2;
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "LD [I], V" << hex << uint16_t(x) << endl;
		#endif
	}
	else
		exception = EXC_UNKNOWN_OPCODE;
}

//LD Vx, [I]
void Chip8::ldVxDeRefI() {
	uint8_t x = (opcode & 0x0f00) >> 8;
	uint8_t fCode = (opcode & 0x000f);
	//Fx6(5)
	if (fCode == 0x5) {
		uint16_t i = I;
		for (uint8_t reg = 0 ; reg <= x ; ++reg, ++i)
			V[reg] = memory[i];
		//I register incremented (LOAD_STORE_QUIRK enabled)
		if (loadStoreQuirk)
			I += (x + 1);
		pc += 2;
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "LD V" << hex << uint16_t(x) << ", [I]" << endl;
		#endif
	}
	else
		exception = EXC_UNKNOWN_OPCODE;
}

//LD Vx, DT
void Chip8::ldVxDt() {
	uint8_t x = (opcode & 0x0f00) >> 8;
	//Fx07
	V[x] = delayTimer;
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "LD V" << hex << uint16_t(x) << ", DT" << endl;
	#endif
}

//LD Vx, K
void Chip8::ldVxK(void) {
	uint8_t x = (opcode & 0x0f00) >> 8;
	//Fx0A
	//We check if any key is pressed and retrieve it
	uint8_t key = gui->getKeyPressed();
	gui->drawFlag = true;
	if (key < KEYNUM) {
		V[x] = key;
		pc += 2;
	}
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "LD V" << hex << uint16_t(x) << ", K" << endl;
	#endif
}

//LD DT, Vx
void Chip8::ldDtVx(void) {
	uint8_t x = (opcode & 0x0f00) >> 8;
	//Fx15
	delayTimer = V[x];
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "LD DT, V" << hex << uint16_t(x) << endl;
	#endif
}

//LD ST, Vx
void Chip8::ldStVx(void) {
	uint8_t x = (opcode & 0x0f00) >> 8;
	//Fx18
	soundTimer = V[x];
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "LD ST, V" << hex << uint16_t(x) << endl;
	#endif
}

//ADD I, Vx
void Chip8::addIVx(void) {
	uint8_t x = (opcode & 0x0f00) >> 8;
	//Fx1E
	int16_t available = END_ADDR - (I + V[x]);
	if (available <= 0)
		I = START_ADDR + abs(available);
	else
		I = I + V[x];
	pc += 2;
	#ifdef DEBUG_DISASM
		disasm.str("");
		disasm.clear();
		disasm << "ADD I, V" << hex << uint16_t(x) << endl;
	#endif
}

//LD R, Vx
void Chip8::ldRVx() {
	uint8_t code = (opcode & 0x000f);
	//Fx75
	if (code == 5) {
		uint8_t x = (opcode & 0x0f00) >> 8;
		//If x > 7 we force x to be 7 (s-chip48 can only save till V7 register included)
		x = (x > 7) ? 7 : x;
		for (uint8_t i = 0 ; i <= x ; ++i)
			R[i] = V[i];

		pc += 2;
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "LD R, V" << hex << uint16_t(x) << endl;
		#endif
	}
	else
		exception = EXC_UNKNOWN_OPCODE;
}

//LD Vx, R
void Chip8::ldVxR() {
	uint8_t code = (opcode & 0x000f);
	//Fx85
	if (code == 5) {
		uint8_t x = (opcode & 0x0f00) >> 8;
		//If x > 7 we force x to be 7 (s-chip48 can only restore till V7 register included)
		x = (x > 7) ? 7 : x;
		for (uint8_t i = 0 ; i <= x ; ++i)
			V[i] = R[i];

		pc += 2;
		#ifdef DEBUG_DISASM
			disasm.str("");
			disasm.clear();
			disasm << "LD V" << hex << uint16_t(x) << ", R" << endl;
		#endif
	}
	else
		exception = EXC_UNKNOWN_OPCODE;
}

void Chip8::initialize() {
	//reset registers
	pc = START_ADDR;
	opcode = 0;
	I = 0;
	sp = SP_BASE_ADDR - 2;
	memset(V, 0, REGNUM);
	memset(R, 0, R_REGNUM);
	//reset ram
	memset(memory, 0, RAMSIZE);
	//reset vram
	memset(gfx, 0, VRAMSIZE);
	//Load fontsets
	memcpy(memory + FONTSET_ADDR, Chip8::chip8_fontset, FONTSET_SIZE);
	memcpy(memory + HI_FONTSET_ADDR, Chip8::schip48_fontset, HI_FONTSET_SIZE);
	//Reset timers
	delayTimer = 0;
	soundTimer = 0;
	//Exception code to zero
	exception = EXC_NO_EXCEPTION;
}

void Chip8::reset() {
	//reset registers
	pc = (biosMode) ? 0 : START_ADDR;
	opcode = 0;
	I = 0;
	sp = SP_BASE_ADDR - 2;
	memset(V, 0, REGNUM);
	//reset vram
	memset(gfx, 0, VRAMSIZE);
	//Reset timers
	delayTimer = 0;
	soundTimer = 0;
	//Exception code to zero
	exception = EXC_NO_EXCEPTION;
	#ifdef DEBUG_RESET
		cout << "RESET" << endl;
	#endif
}

//Load Chip8 machine code program into memory (from START_ADDR)
//returns the total bytes read (program size)
uint16_t Chip8::load(const char *name, bool biosload) {
	char buf[512];
	uint16_t i = 0;
	romPath.assign(name);

	memset(buf, 0, 512);
	ifstream data(name, ifstream::binary);
	if (data) {
		while (!data.eof()) {
			data.read(buf, 512);
			uint16_t bytesRead = data.gcount();
			if (!biosload)
				memcpy(memory + START_ADDR + i, buf, bytesRead);
			else {
				memcpy(memory + i, buf, bytesRead);
				biosMode = true;
				pc = 0; //C8pp fake bios is loaded to 0 address
			}
			i += bytesRead;
		}
	}
	else
		cout << "Error opening file: " << name << endl;
	data.close();
	return i;
}

void Chip8::dumpMem(uint16_t begin, uint16_t end) {
	cout << "**** RAM Dump ****" << endl;
	if (begin < RAMSIZE && end < RAMSIZE) {
		for (int i = begin ; i <= end ; ++i)
			cout << "0x" << hex << i << ": " << "0x" << hex << uint16_t(memory[i]) << endl;
	}
	else
		cout << "dumpMem(): cannot read out of memory" << endl;
}

void Chip8::dumpVmem(uint16_t begin, uint16_t end) {
	cout << "**** VRAM Dump ****" << endl;
	if (begin < VRAMSIZE && end < VRAMSIZE) {
		for (int i = begin ; i <= end ; ++i)
			cout << "0x" << hex << i << ": " << "0x" << hex << uint16_t(gfx[i]) << endl;
	}
	else
		cout << "dumpVMem(): cannot read out of video memory" << endl;
}

void Chip8::dumpCpuStatus() {
	cout << "**** Cpu Status ****" << endl;
	//PC, I, SP
	cout << "PC: 0x" << hex << pc << "\t I: 0x" << hex << I << "\t SP: 0x" << hex << sp << endl;
	//V0 - VF
	for (int i = 0 ; i < REGNUM ; ++i)
		cout << "V" << hex << i << ": 0x" << hex << uint16_t(V[i]) << endl;
	//R0 - R7
	for (int i = 0 ; i < R_REGNUM ; ++i)
		cout << "R" << hex << i << ": 0x" << hex << uint16_t(R[i]) << endl;
	//Timers
	cout << "DelayTimer: " << uint16_t(delayTimer) << endl;
	cout << "SoundTimer: " << uint16_t(soundTimer) << endl;

	//Last opcode

	//We need to prefix '0's to show in a better way the opcode hex number
	string prefix;
	uint8_t nDigits = 0;
	for (uint16_t tmpOpcode = opcode ; tmpOpcode > 0 ; tmpOpcode /= 0x10)
		++nDigits;

	//Append '0' to prefix
	for (uint8_t i = 0 ; i < 4 - nDigits ; ++i)
		prefix += '0';

	cout << "Last opcode: 0x" << prefix << hex << opcode << endl;
}

//Fetch, decode and execute the opcode (1 clock cycle)
int Chip8::emulateCycle() {
	//Fetch
	if (pc >= RAMSIZE) {
		cout << "Reached end of Memory, resetting PC register to 0x" << hex << START_ADDR << endl;
		pc = START_ADDR;
	}

	opcode = uint16_t(memory[pc]) << 8 | uint16_t(memory[pc + 1]);

	//Decode and Execute
	//We get the first 4 bits to select instructions group
	ptrMethodChip8 execute = Chip8::chip8Table[(opcode & 0xf000) >> 12];

	if (execute)
		(this->*execute)();
	else
		exception = EXC_UNKNOWN_OPCODE;

	return exception;
}

//Update timers (called every 16ms, 60Hz timer)
void Chip8::updateTimers() {
	if(delayTimer > 0)
		--delayTimer;

	if(soundTimer > 0)
	{
		if (soundTimer == 1) {
			gui->doBeep();
			#ifdef DEBUG_BEEP
				cout << "BEEP" << endl;
			#endif
		}
		--soundTimer;
	}
	#ifdef DEBUG_TIMERS
		cout << "Delay Timer: 0x" << uint16_t(delayTimer) << endl;
		cout << "Sound Timer: 0x" << uint16_t(soundTimer) << endl;
	#endif
}

//Return video memory array
uint8_t *Chip8::getGfx() {
	return gfx;
}

//Return current program counter value
uint16_t Chip8::getPc() {
	return pc;
}

//Returns the current opcode disassembly
string Chip8::getDisasm() {
	return disasm.str();
}

uint8_t Chip8::getV(uint8_t x) {
	return V[x];
}

uint8_t Chip8::getR(uint8_t x) {
	return R[x];
}

uint16_t Chip8::getI() {
	return I;
}

uint8_t Chip8::getDT() {
	return delayTimer;
}

uint8_t Chip8::getST() {
	return soundTimer;
}

uint16_t Chip8::getSP() {
	return sp;
}

void Chip8::setV(uint8_t x, uint8_t val) {
	V[x] = val;
}

void Chip8::setR(uint8_t x, uint8_t val) {
	R[x] = val;
}

void Chip8::setI(uint16_t val) {
	if (val < RAMSIZE)
		I = val;
	else
		cout << "setI(): cannot set I register out of memory location" << endl;
}

void Chip8::setDT(uint8_t val) {
	delayTimer = val;
}

void Chip8::setST(uint8_t val) {
	soundTimer = val;
}

void Chip8::setSP(uint16_t val) {
	if (val >= SP_BASE_ADDR - 2 && val <= SP_MAX_ADDR)
		sp = val;
	else
		cout << "setSP(): cannot set SP register out of stack area" << endl;
}

void Chip8::poke(uint16_t begin, uint16_t end, uint8_t val) {
	if (begin < RAMSIZE && end < RAMSIZE) {
		for (int i = begin ; i <= end ; ++i)
			memory[i] = val;
	}
	else
		cout << "poke(): cannot write out of memory" << endl;
}

uint8_t Chip8::peek(uint16_t address) {
	uint8_t byte;
	if (address < RAMSIZE)
		byte = memory[address];
	else {
		cout << "peek(): cannot read out of memory" << endl;
		byte = 0;
	}
	return byte;
}

void Chip8::vpoke(uint16_t begin, uint16_t end, uint8_t val) {
	if (begin < VRAMSIZE && end < VRAMSIZE) {
		for (int i = begin ; i <= end ; ++i)
			gfx[i] = val;
	}
	else
		cout << "dumpVMem(): cannot write out of video memory" << endl;
}

uint8_t Chip8::vpeek(uint16_t address) {
	uint8_t byte;
	if (address < VRAMSIZE)
		byte = gfx[address];
	else {
		cout << "vpeek(): cannot read out of video memory" << endl;
		byte = 0;
	}
	return byte;
}

uint16_t Chip8::getGfxWidth() {
	return gui->gfxWidth;
}

uint16_t Chip8::getGfxHeight() {
	return gui->gfxHeight;
}
