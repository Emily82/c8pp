#include <video.h>
#include <cstring>

Video::Video(uint8_t resWidth, uint8_t resHeight) {
	powerOff = false;
	reset = false;
	scale = DEFAULT_SCALE;
	gfxWidth = resWidth;
	gfxHeight = resHeight;
	screenWidth = gfxWidth * scale;
	screenHeight = gfxHeight * scale;
	//Nothing else to do here
	memset(keyStatus, false, KEYNUM);
}

bool Video::onPowerOff() {
	return powerOff;
}

void Video::togglePause() {
	paused = (paused) ? false : true;
}

bool Video::onPause() {
	return paused;
}

bool Video::onReset() {
	return reset;
}

void Video::setScale(uint8_t scale) {
	//Libraries (ex. videoSDL) will override this method with some extra code for new window size
	this->scale = scale;
	//Update screenWidth and screenHeight
	screenWidth = gfxWidth * scale;
	screenHeight = gfxHeight * scale;
}

uint8_t Video::getScale(void) {
	return scale;
}

void Video::setScreenMode(uint8_t mode) {
	drawFlag = true;
	screenMode = mode;
}

uint8_t Video::getScreenMode() {
	return screenMode;
}
