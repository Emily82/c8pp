#include <videoSDL.h>
#include <iostream>

//#define DEBUG_TEXTURE //Debug info when updating screen
//#define DEBUG_SCREEN_RESOLUTION //Debug info when changing resolution

using namespace std;

VideoSDL::VideoSDL(uint8_t resWidth, uint8_t resHeight) : Video(resWidth, resHeight) {
	gWindow = NULL;
	renderer = NULL;
	texture = NULL;
	//Inherited from Video class
	//Emulator key binding (SDLKey => emulator key)
	keyBinding = {
		{SDLK_1, 0x1},
		{SDLK_2, 0x2},
		{SDLK_3, 0x3},
		{SDLK_4, 0xc},
		{SDLK_q, 0x4},
		{SDLK_w, 0x5},
		{SDLK_e, 0x6},
		{SDLK_r, 0xd},
		{SDLK_a, 0x7},
		{SDLK_s, 0x8},
		{SDLK_d, 0x9},
		{SDLK_f, 0xe},
		{SDLK_z, 0xa},
		{SDLK_x, 0x0},
		{SDLK_c, 0xb},
		{SDLK_v, 0xf}
	};
	//Emulator key pressed will be stored here.
	//If no key is pressed its value is 0xff
	keyPressed = 0xff;
	drawFlag = true;
	setUpdateArea(0, 0, gfxWidth, gfxHeight);
	//setUpdateArea(0, 0, 32, 16);
}

//Screen area to update
void VideoSDL::setUpdateArea(int x, int y, int w, int h) {
	updateArea.x = x;
	updateArea.y = y;
	updateArea.w = w;
	updateArea.h = h;
}

uint8_t VideoSDL::getKeyPressed() {
	return keyPressed;
}

void VideoSDL::quit() {
	//Free video system
	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
    renderer = NULL;
	//Free sound system
	SDL_CloseAudioDevice(deviceId);
	SDL_FreeWAV(wavBuffer);
	//Signal stop game loop (in main)
	powerOff = true;
	//Quit SDL subsystems
	SDL_Quit();
}

//True if fullscreen enabled, false otherwise
bool VideoSDL::getFullScreenStatus() {
	return (FULLSCREEN & SDL_GetWindowFlags(gWindow));
}

void VideoSDL::inputEvents() {
	while( SDL_PollEvent( &event ) != 0 )
	{
		//User requests quit
		if (event.type == SDL_QUIT)
			quit();
		//Keyboard requests
		if (event.type == SDL_KEYDOWN) {
			uint32_t key = event.key.keysym.sym;

			//Emulator hex keyboard keys
			//We check if the key exists in binding map
			if (keyBinding.count(key) > 0) {
				uint8_t emuKey = keyBinding[key];
				keyStatus[emuKey] = true;
				//We store this key to keyPressed
				keyPressed = emuKey;
			}
			else {
				//Emulator setup keys (ESC: quit, soft reset, ecc ...)
				switch(key) {
					case SDLK_ESCAPE:
						quit();
					break;
					case SDLK_F12:
						reset = true;
					break;
					default:
					break;
				}
			}
		}
		if (event.type == SDL_KEYUP) {
			uint32_t key = event.key.keysym.sym;
			//Emulator hex keyboard keys
			//We check if the key exists in binding map
			if (keyBinding.count(key) > 0) {
				uint8_t emuKey = keyBinding[key];
				keyStatus[emuKey] = false;
				//We reset keyPressed
				keyPressed = 0xff;
			}
			else {
				bool fullscreenStatus = getFullScreenStatus();
				//Emulator setup keys (ESC: quit, soft reset, ecc ...)
				switch(key) {
					case SDLK_F12:
						reset = false;
					break;
					case SDLK_F10:
						(fullscreenStatus)? disableFullScreen() : enableFullScreen();
						drawFlag = true;
					break;
					case SDLK_p:
						togglePause();
					break;
					default:
					break;
				}
			}
		}
	}
}

//FORMAT: 1 bit per pixel
void VideoSDL::updateScreen(uint8_t *gfx) {
	#ifdef DEBUG_TEXTURE
		cout << "Area: x = " << updateArea.x << ", y = " << updateArea.y << ", w = " << updateArea.w;
		cout << ", h = " << updateArea.h << endl;
	#endif
 	if (SDL_SetRenderTarget(renderer, texture) != 0)
		cout << "drawGraphics(): Texture cannot be target, enable SDL_TEXTUREACCESS_TARGET flag!" << endl;
	else {
		for(int y = 0 ; y < gfxHeight ; y++)
		{
			for(int x = 0 ; x < gfxWidth ; x++)
			{
				uint16_t totalBits = x + y * gfxWidth;
				uint16_t index = totalBits / 8;
				uint8_t offset = (totalBits % 8);
				uint8_t pixel = gfx[index] & (0x80 >> offset);
				if(pixel == 0)
					SDL_SetRenderDrawColor(renderer, PIXEL_OFF);
				else
					SDL_SetRenderDrawColor(renderer, PIXEL_ON);

				SDL_RenderDrawPoint(renderer, x, y);
			}
		}

		SDL_SetRenderTarget(renderer, NULL);
		SDL_RenderClear(renderer);
		SDL_RenderCopy(renderer, texture, NULL, NULL);
		SDL_RenderPresent(renderer);
	}
}

bool VideoSDL::initSDL() {
	//Initialization flag
	bool success = true;

	//Initialize SDL2
	if( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_AUDIO ) < 0 )
    {
        printf( "init(): SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
        success = false;
    }
    else
    {
        //Create window
        gWindow = SDL_CreateWindow( "C8pp", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, SDL_WINDOW_SHOWN );
        if( gWindow == NULL )
        {
            printf( "init(): Window could not be created! SDL_Error: %s\n", SDL_GetError() );
            success = false;
        }

		//Initialize SDL audio
		SDL_LoadWAV("beep.wav", &wavSpec, &wavBuffer, &wavLength);
		deviceId = SDL_OpenAudioDevice(NULL, 0, &wavSpec, NULL, 0);
    }

	renderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);
	texture = SDL_CreateTexture(renderer,
								SDL_PIXELFORMAT_ARGB8888,
							    SDL_TEXTUREACCESS_TARGET,
							    gfxWidth,
							    gfxHeight);

    return success;
}

void VideoSDL::setScale(uint8_t scale) {
	Video::setScale(scale);
	SDL_SetWindowSize(gWindow, screenWidth, screenHeight);
}

void VideoSDL::setResolution(uint8_t resWidth, uint8_t resHeight) {
	gfxWidth = resWidth;
	gfxHeight = resHeight;
	//Update screenWidth and screenHeight
	screenWidth = gfxWidth * scale;
	screenHeight = gfxHeight * scale;

	//Check if fullscreen is enabled or not before deleting and creating new window
	bool isFullScreen = getFullScreenStatus();

	//Free video system
	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
    renderer = NULL;

	//Free sound system
	SDL_CloseAudioDevice(deviceId);
	SDL_FreeWAV(wavBuffer);

	//Init back all with new resolution
	initSDL();

	//If window was fullscreen then new one should be too
	if (isFullScreen)
		enableFullScreen();

	//Needed to fix a bug with mouse when changing resolution
	if (SDL_GetRelativeMouseMode())
	{
		SDL_SetRelativeMouseMode(SDL_FALSE);
		SDL_SetRelativeMouseMode(SDL_TRUE);
	}
	#ifdef DEBUG_SCREEN_RESOLUTION
		int w, h;
		SDL_GetWindowSize(gWindow, &w, &h);
		cout << "New window width: " << w << endl;
		cout << "New window height: " << h << endl;
	#endif
}

void VideoSDL::doBeep() {
	SDL_QueueAudio(deviceId, wavBuffer, wavLength);
	SDL_PauseAudioDevice(deviceId, 0);
}

int VideoSDL::enableFullScreen() {
	SDL_ShowCursor(SDL_DISABLE);
	return SDL_SetWindowFullscreen(gWindow, FULLSCREEN);
}


int VideoSDL::disableFullScreen() {
	SDL_ShowCursor(SDL_ENABLE);
	return SDL_SetWindowFullscreen(gWindow, 0);
}

void VideoSDL::showScreen() {
	SDL_ShowWindow(gWindow);
}

void VideoSDL::hideScreen() {
	SDL_HideWindow(gWindow);
}
