    0000  00000  00000  00000
    0     0   0  0   0  0   0
    0     00000  00000  00000
    0     0   0  0      0
    0000  00000  0      0

# Chip8 / S-Chip48 emulator

### C8PP Dependencies
To build c8pp you need these libraries installed in your OS:
- libsdl2
- libsdl2-dev

For Linux distros, debian based

    sudo apt-get install libsdl2-2.0-0 libsdl2-dev

### Building

To compile source, just enter c8pp directory in terminal and do:

    make

To clean all builds:

    make clean

### Running

This command will run a font test program ;)

    ./c8pp -r examples/fonts.c8

### Options

#### -r romfile
Loads the specified rom file to address 0x200. If this option is not specified or rom file does not exist, then a bios will be booted. (bios and source code available in bios/ folder)

    -q feature1 feature2 ...
Enables quirk features needed for some rom to work properly.

Current supported parameters:

- **shift**
Shifts Vx register by 1 leaving Vy unchanged, opcodes: **8xy6, 8xyE**;
- **loadstore**
I register is altered after operations of opcodes: **Fx55, Fx65**;
- **packedbcd**
One NIBBLE for every digit instead of one byte. Opcode: **Fx33**;
- **imagewrap**
When drawing sprites out of the screen, emulator will automatically redraw them to the opposite side of the screen, opcodes: **Dxyn, Dxy0**.

Example:

    ./chip8 -q shift loadstore -r rom.c8

Enables both shift and loadstore quirks.
Read pdf documentation in **docs/** folder for more details about chip8 instruction set.

#### -s scale number
Set scale value for output screen size.
Default scale value is 10.

Example:

    ./c8pp -r rom.c8 -s 20

#### -c multiplier number
Set cpu frequency multiplier.
Default value is 1.

Example:

	./chip8 -r rom.c8 -c 4

Chip8 cpu will clock at (1000 x multiplier) Hz speed (~ 4 kHz).

**NOTE**: option argument can be a fraction, so if it is 0.5, cpu frequency will be the half of default value (i.e. ~ 500Hz).

#### -f
Enable fullscreen.

### Emulator Hex Keyboard

    +---------------+--------------+
    | C8 keyboard   | PC keyboard  |
    +---------------+--------------+
    |       0       |      X       |
    |       1       |      1       |
    |       2       |      2       |
    |       3       |      3       |
    |       4       |      Q       |
    |       5       |      W       |
    |       6       |      E       |
    |       7       |      A       |
    |       8       |      S       |
    |       9       |      D       |
    |       A       |      Z       |
    |       B       |      C       |
    |       C       |      4       |
    |       D       |      R       |
    |       E       |      F       |
    |       F       |      V       |
    +------------------------------+

### Emulator Keyboard commands

- **ESC**: Close emulator;
- **F12**: Soft reset (no clear ram);
- **F10**: Toggle fullscreen;
- **P**: Pause emulator;

### Debugger
C8pp ships a debugger with cli to manipulate the emulator in runtime.
To build debugger from sources type:

    make debug

You will see an executable **dbgc8pp**.
This application accepts the same **c8pp** parameters.
To open debugger just type:

    ./dbgc8pp -r examples/fonts.c8

You will still execute font test program but using debugger.
Command line interface is similar to popular gdb.

### Debugger Command Reference

- All numbers / addresses are in hex form even without '0x' prefix;
- Blank command will execute the last command.
- All register names and hex letters can be both lowercase and uppercase (ex. Vf = vf = VF = vF)

#### Commands

- **cont** : Continue execution till next breakpoint;
Short Form: **c**

- **step** :	Do only one clock cycle execution and print disassembly of last opcode.
Short Form: **s**

- **break** : Add new breakpoint address. All address list will be sorted.
Short Form: **b**

- **breakif Vx=byte** : Set conditional breakpoint for Vx register (x from 0 to f). When Vx=byte the emulation will pause.
Short Form: **b Vx=byte**

- **breakif Rx=byte** : Set conditional breakpoint for Rx register (x from 0 to f). When Rx=byte the emulation will pause.
Short Form: **b Rx=byte**

- **breakif I=address** : Set conditional breakpoint for I register. When I=adress, the emulation will pause.
Short Form: **b I=address**

- **breakif SP=address** : Set conditional breakpoint for SP register. When SP=address, the emulation will pause.
Short Form: **b SP=address**

- **breakif DT=byte** : Set conditional breakpoint for DT register. When DT=byte, the emulation will pause.
Short Form: **b DT=byte**

- **breakif ST=byte** : Set conditional breakpoint for ST register. When ST=byte, the emulation will pause.
Short Form: **b ST=byte**

- **breaklist** : Print breakpoint address list.
Short Form: **bl**

- **breakiflist** : Print conditional breakpoint list.
Short Form: **bi**

- **printcpu** : Print all cpu registers.
Short Form: **p**

- **quit** : Exit from debugger and close emulator.
Short Form: **q**

- **disasm** : Print disassembly of last opcode.
Short Form: **d**

- **printm s_addr [e addr]** : Print memory ram content from s_addr address to e_addr. If e_addr is not specified then only s_addr location will be printed.
Short Form: **p s_addr [e_addr]**

- **printv s_addr [e_addr]** : Print video memory content from s_addr to end_addr. If end_addr is not specified then only s_addr location will be printed.
Short Form: **v s_addr [e_addr]**

- **print Vx** : Print Vx register value (x from 0 to f).
Short Form: **p Vx**

- **print Rx** : Print Rx register value (x from 0 to f).
Short Form: **p Rx**

- **print I** : Print I register value.
Short Form: **p I**

- **print SP** : Print SP register value.
Short Form: **p SP**

- **print DT** : Print DT register value.
Short Form: **p DT**

- **print ST** : Print ST register value.
Short Form: **p ST**

- **unbreak** : Remove all address breakpoint list.
Short Form: **u**

- **unbreakif Vx** : Remove conditional breakpoint for Vx register.
Short Form: **u Vx**

- **unbreakif Rx** : Remove conditional breakpoint for Rx register.
Short Form: **u Rx**

- **unbreakif I** : Remove conditional breakpoint for I register.
Short Form: **u I**

- **unbreakif SP** : Remove conditional breakpoint for SP register.
Short Form: **u SP**

- **unbreakif DT** : Remove conditional breakpoint for DT register.
Short Form: **u DT**

- **unbreakif ST** : Remove conditional breakpoint for ST register.
Short Form: **u ST**

- **set Vx=byte** : Set Vx register to byte value.
Short Form: **s Vx=byte**

- **set Rx=byte**	: Set Rx register to byte value.
Short Form: **s Rx=byte**

- **set I=address** : Set I register to address value.
Short Form: **s I=address**

- **set SP=address** : Set SP register to address value.
Short Form: **s SP=address**

- **set DT=byte** : Set DT register to byte value.
Short Form: **s DT=byte**

- **set ST=byte** : Set ST register to byte value.
Short Form: **s ST=byte**

- **poke s_addr [e_addr]=byte** : Write byte value from s_addr to e_addr in memory ram. If e_addr is not specified then
byte will be written only to s_addr location.
Short Form: **pk s_addr [e_addr]=byte**

- **vpoke s_addr [e_addr]=byte** : Write byte value from s_addr to e_addr in video memory. If e_addr is not specified then
byte will be written only to s_addr location.
Short Form: **vpk s_addr [e_addr]=byte**

- **setpixel x y** : Set pixel to x y screen coordinate. If pixel is already set then Vf register will be set to 1, and the pixel will be cleared (XOR operation).
Short Form: **setp x y**

- **clearpixel x y** : Clear pixel to x y screen coordinate.
Short Form: **clrp x y**



*Written by Emily Fiorentini, many thanks to open source community ;)*
