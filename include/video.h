#ifndef VIDEO_H
#define VIDEO_H

#define DEFAULT_GFX_WIDTH 	64
#define DEFAULT_GFX_HEIGHT 	32
#define HIGH_GFX_WIDTH		128
#define HIGH_GFX_HEIGHT		64
#define DEFAULT_SCALE 		10
#define KEYNUM 				16

#define COLOR(RED, GREEN, BLUE, ALPHA)	RED, GREEN, BLUE, ALPHA

#define PIXEL_ON 	COLOR(0x00, 0x00, 0x00, 0xff)
#define PIXEL_OFF 	COLOR(0x77, 0x8c, 0x7c, 0xff)

#include <cstdint>
#include <map>

using namespace std;

class Video {
public:
	Video(uint8_t resWidth, uint8_t resHeight);
	virtual void inputEvents(void) = 0;
	virtual void updateScreen(uint8_t *gfx) = 0;
	virtual uint8_t getKeyPressed(void) = 0;
	virtual void setUpdateArea(int x, int y, int w, int h) = 0;
	virtual int enableFullScreen(void) = 0;
	virtual int disableFullScreen(void) = 0;
	virtual bool getFullScreenStatus(void) = 0;
	virtual void doBeep(void) = 0;
	virtual void setScale(uint8_t scale);
	virtual void setResolution(uint8_t resWidth, uint8_t resHeight) = 0;
	virtual void quit(void) = 0;
	virtual void showScreen(void) = 0;
	virtual void hideScreen(void) = 0;

	uint8_t getScale(void);
	uint8_t keyStatus[KEYNUM];
	bool onPowerOff(void);
	void togglePause(void);
	bool onPause(void);
	bool onReset(void);
	void setScreenMode(uint8_t mode);
	uint8_t getScreenMode(void);
	//Flag to update the screen
	bool drawFlag;
	uint8_t gfxWidth;
	uint8_t gfxHeight;
protected:
	//Keyboard
	bool powerOff;
	bool paused;
	bool reset;
	uint8_t keyPressed;
	map<uint8_t, uint8_t> keyBinding;
	uint16_t screenWidth;
	uint16_t screenHeight;
	uint8_t scale;
	uint8_t screenMode;
};

enum ScreenMode
{
	NORMAL, // 64 x 32
	HIGH    //128 x 64
};
#endif
