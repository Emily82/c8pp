#ifndef CHIP8_H
#define CHIP8_H

#include "video.h"
#include <cstdint>
#include <sstream>

#define RAMSIZE 	4096
#define REGNUM 		16
#define R_REGNUM	8

#define VRAMSIZE 	2048
#define TIMER_FREQ 	60 //Hz
#define KEYNUM 	   	16

#define START_ADDR 	0x200 //lower 512 bytes are reserved (fontset)
#define END_ADDR	0xEDE //upper 32 + 256 bytes are reserved (stack and video refresh)

#define STACKLEVELS 	16 //stack size
#define SP_BASE_ADDR 	0xEDF //starting address of stack
#define SP_MAX_ADDR 	SP_BASE_ADDR + STACKLEVELS * 2 //max address for stack

#define FONTSET_ADDR 	0x110
#define FONTSET_SIZE 	80
#define FONT_SIZE 		5

#define HI_FONTSET_ADDR 	0x160
#define HI_FONTSET_SIZE 	160
#define HI_FONT_SIZE 		10

#define GETFONT_ADDR(FONT) 		FONTSET_ADDR + (FONT * FONT_SIZE)
#define GETHI_FONT_ADDR(FONT) 	HI_FONTSET_ADDR + (FONT * HI_FONT_SIZE)

#define CHIP8_TABLE_SIZE 		16
#define CHIP8_GRP8_SIZE 		16
#define CHIP8_GRP0_SIZE 		16
#define CHIP8_GRP0_GRPE_SIZE 	16
#define CHIP8_GRP0_GRPF_SIZE 	16
#define CHIP8_GRPF_SIZE 		16
#define CHIP8_GRPF_GRP0_SIZE 	16
#define CHIP8_GRPF_GRP1_SIZE 	16
#define CHIP8_GRPF_GRP3_SIZE 	16
#define NUM_KEYS 16

#define FAKEBIOS	"bios/bios.c8"

//Uncomment to enable debug features or use -D option in Makefile
//#define DEBUG_DISASM //Activate disassembly printout of cpu code execution
//#define DEBUG_SHOW_GRPSEL //Show printout debug when cpu selects a group of instructions
//#define DEBUG_TIMERS //Activate timers value printout
//#define DEBUG_GFX //Show debug info when using chip8 drw instruction
//#define DEBUG_SCROLLDOWN //Show debug info about scdn instruction (00Cn)
//#define DEBUG_RND //Show generated number when calling RND opcode
//#define DEBUG_RESET //Show a printout when resetting emulator
//#define DEBUG_BEEP //show BEEP to terminal when a beep occurs
//#define DEBUG_COLLISION

class Chip8;

//We Define pointer to member function of Chip8 class instance type, returning void with no args
typedef void(Chip8::*ptrMethodChip8)(void);

class Chip8 {
public:
	Chip8(Video *gui);
	void initialize(void);
	void dumpMem(uint16_t begin = 0, uint16_t end = RAMSIZE - 1);
	void dumpVmem(uint16_t begin = 0, uint16_t end = VRAMSIZE - 1);
	void dumpCpuStatus(void);
	uint16_t load(const char *name, bool biosload = false);
	int emulateCycle(void);
	void updateTimers(void);
	uint8_t *getGfx(void);
	void reset(void);
	uint16_t getPc(void);
	string getDisasm(void);
	uint8_t getV(uint8_t x);
	uint8_t getR(uint8_t x);
	uint16_t getI(void);
	uint8_t getDT(void);
	uint8_t getST(void);
	uint16_t getSP(void);
	void setV(uint8_t x, uint8_t val);
	void setR(uint8_t x, uint8_t val);
	void setI(uint16_t val);
	void setDT(uint8_t val);
	void setST(uint8_t val);
	void setSP(uint16_t val);
	void poke(uint16_t begin, uint16_t end, uint8_t val);
	uint8_t peek(uint16_t address);
	void vpoke(uint16_t begin, uint16_t end, uint8_t val);
	uint8_t vpeek(uint16_t address);
	uint16_t getGfxWidth(void);
	uint16_t getGfxHeight(void);
	//Features needed for some rom
	bool shiftQuirk;
	bool loadStoreQuirk;
	bool packedBcdQuirk;
	bool imageWrapQuirk;
protected:
	//Fontset
	static uint8_t chip8_fontset[FONTSET_SIZE];
	static uint8_t schip48_fontset[HI_FONTSET_SIZE];
	//Instruction tables
	ptrMethodChip8 chip8Table[CHIP8_TABLE_SIZE];
	ptrMethodChip8 chip8grp8[CHIP8_GRP8_SIZE];
	ptrMethodChip8 chip8grp0[CHIP8_GRP0_SIZE];
	ptrMethodChip8 chip8grp0grpE[CHIP8_GRP0_GRPE_SIZE];
	ptrMethodChip8 chip8grp0grpF[CHIP8_GRP0_GRPF_SIZE];
	ptrMethodChip8 chip8grpF[CHIP8_GRPF_SIZE];
	ptrMethodChip8 chip8grpFgrp0[CHIP8_GRPF_GRP0_SIZE];
	ptrMethodChip8 chip8grpFgrp1[CHIP8_GRPF_GRP1_SIZE];
	ptrMethodChip8 chip8grpFgrp3[CHIP8_GRPF_GRP3_SIZE];
	//Group selectors
	void grp0(void);
	void grp0grpE(void);
	void grp0grpF(void);
	void grp8(void);
	void grpF(void);
	void grpFgrp0(void);
	void grpFgrp1(void);
	void grpFgrp3(void);
	//Chip8 Instructions
	void cls(void);
	void ret(void);
	void cpuNull(void);
	void call(void);
	void jp(void);
	void seVxByte(void);
	void sneVxByte(void);
	void seVxVy(void);
	void addVxByte(void);
	void ldIAddr(void);
	void jpV0Addr(void);
	void rndVxByte(void);
	void drwVxVyNibble(void);
	void ldVxByte(void);
	void ldVxVy(void);
	void orVxVy(void);
	void andVxVy(void);
	void xorVxVy(void);
	void addVxVy(void);
	void subVxVy(void);
	void shrVx(void);
	void subnVxVy(void);
	void shlVx(void);
	void sneVxVy(void);
	void skip(void);
	void ldFVx(void);
	void ldBVx(void);
	void ldDeRefIVx(void);
	void ldVxDeRefI(void);
	void ldVxDt(void);
	void ldVxK(void);
	void ldDtVx(void);
	void ldStVx(void);
	void addIVx(void);
	//Super Chip48 instructions
	void scdn(void);
	void scr(void);
	void scl(void);
	void exit(void);
	void low(void);
	void high(void);
	void ldHfVx(void);
	void ldRVx(void);
	void ldVxR(void);
private:
	uint16_t opcode;
	stringstream disasm; //This string contains the disasm of current opcode
	//RAM
	uint8_t memory[RAMSIZE];
	//VRAM
	uint8_t gfx[VRAMSIZE];
	//CPU registers
	uint8_t V[REGNUM];
	uint8_t R[R_REGNUM]; //Save-restore registers (s-chip48)
	uint16_t I;
	uint16_t pc;
	//Timers
	uint8_t delayTimer;
	uint8_t soundTimer;
	//Stack
	uint16_t sp;
	//Exception code
	int exception;
	//Pointer to instance of video API
	Video *gui;
	//Last rom namepath loaded
	string romPath;
	bool paused;
	bool biosMode;
};

enum Exceptions
{
    EXC_NO_EXCEPTION,
    EXC_UNKNOWN_OPCODE,
    EXC_EMPTY_STACK,
    EXC_FULL_STACK,
	EXC_OUT_OF_MEMORY
};

enum VideoLibrary
{
	SDL,
	OPENGL //Not implemented yet
};

#endif
