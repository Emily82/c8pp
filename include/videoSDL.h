#ifndef VIDEOSDL_H
#define VIDEOSDL_H

#include <SDL2/SDL.h>
#include "video.h"

//Fake fullscreen mode: window has got same screen size, best solution for now
#define FULLSCREEN SDL_WINDOW_FULLSCREEN_DESKTOP
//Real fullscreen mode: disabled for now cause its buggy SDL2 feature
//#define FULLSCREEN SDL_WINDOW_FULLSCREEN

using namespace std;

class VideoSDL : public Video {
public:
	VideoSDL(uint8_t resWidth, uint8_t resHeight);
	void inputEvents(void) override;
	void updateScreen(uint8_t *gfx) override;
	uint8_t getKeyPressed(void) override;
	void setUpdateArea(int x, int y, int w, int h) override;
	int enableFullScreen(void) override;
	int disableFullScreen(void) override;
	bool getFullScreenStatus(void) override;
	void setScale(uint8_t scale) override;
	void doBeep(void) override;
	void quit(void) override;
	void setResolution(uint8_t resWidth, uint8_t resHeight) override;
	void showScreen(void) override;
	void hideScreen(void) override;
	bool initSDL(void);
private:
	//Video
	SDL_Rect updateArea;
	SDL_Event event;
	SDL_Window* gWindow;
	SDL_Renderer* renderer;
	SDL_Texture* texture;
	//Audio
	SDL_AudioSpec wavSpec;
	Uint32 wavLength;
	Uint8 *wavBuffer;
	SDL_AudioDeviceID deviceId;
};
#endif
