#ifndef SHELL_H
#define SHELL_H

#include <c8pp.h>
#include <iostream>
#include <sstream>
#include <video.h>
#include <cstdint>
#include <string>
#include <algorithm>
#include <regex>

using namespace std;

enum ShellCommands {
	CONT,
	STEP,
	QUIT
};

//Debug features
//#define DEBUG_DUMPCPU
//#define DEBUG_MEM
//#define DEBUG_VMEM
//#define DEBUG_SHOW_CPU_FREQ

class Shell {
public:
	Shell(Chip8 *myChip8, Video *gui);
	int dbgShell(uint16_t currentAddr);
	void printAddressBreakPoints(void);
	void printConditionBreakPoints(void);
	uint16_t countSpaces(string s);
	bool checkAddressBreakpoints(void);
	bool checkConditionBreakpoints(void);
	void printHelp(string command = "");

	vector<uint16_t> breakpoints;
	uint16_t Vbreakpoints[REGNUM];
	uint16_t Rbreakpoints[R_REGNUM];
	uint16_t Ibreakpoint;
	uint16_t SPbreakpoint;
	uint16_t DTbreakpoint;
	uint16_t STbreakpoint;
private:
	uint16_t getNumber(string s, regex rule, uint16_t *pos = nullptr);
	string getParam(string s, regex rule, uint16_t *pos = nullptr);
	uint16_t countTokens(string s);

	string cmd;
	string lastcmd;
	Chip8 *myChip8;
	Video *gui;
};

#endif
