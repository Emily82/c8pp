#ifndef UTILS_H
#define UTILS_H

#include <c8pp.h>
#include <video.h>
#include <chrono>
#include <ctime>
#include <fstream>

#define TIMERS_DELAY 16667 //microseconds, 60hz ~ 16667 microseconds - 0.016 seconds
#define BASE_FREQ 1000 //Hz
#define DEFAULT_SLEEP_DELAY 880 //microseconds

//Get now time point
#define NOW() chrono::system_clock::now()
//Calculate duration time between 2 time points
#define INTERVAL(START, END) chrono::duration_cast<chrono::microseconds> (END - START)
#define DURATION(START, END) INTERVAL(START, END).count()
//Convert microseconds to seconds
#define USTOS(TIME) (TIME / 1000000)
//Convert seconds to microseconds
#define STOUS(TIME) (TIME * 1000000)
//Convert microseconds to milliseconds
#define USTOMS(TIME) (TIME / 1000)
//Convert milliseconds to microseconds
#define MSTOUS(TIME) (TIME * 1000)
//Convert nanoseconds to microseconds
#define NSTOUS(TIME) (TIME / 1000)
//Convert microseconds to nanoseconds
#define USTONS(TIME) (TIME * 1000)

typedef chrono::time_point<chrono::system_clock> TimePoint;
typedef chrono::microseconds uSeconds;

extern uint32_t sleepDelay; //microseconds

int optSetup(int argc, char *argv[], Chip8 *myChip8, Video *gui);
bool fileExistsAndNotDir(const char *filename);
void checkCpuExceptions(Chip8 *myChip8, int errorCode);

enum OptionErrors {
	NOERR,
	NOROM,
	ERR
};

#endif
