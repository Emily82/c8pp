	jp main
drawHex:
	; va -> hex to draw
	; v0 -> x coordinate
	; v1 -> y coordinate
	ld	f, va
	drw v0, v1, 5
	ret
drawChar:
	; i -> font address
	; v0 -> x coordinate
	; v1 -> y coordinate
	drw v0, v1, 5
	ret
drawLogo:
	; v0 -> x coordinate
	; v1 -> y coordinate
	ld	va, #c ; print charcater C
	call drawHex
	add v0, 6
	ld	va, #8 ; print charcater 8
	call drawHex
	add v0, 6
	ld	i,	fontP ; print character P
	call drawChar
	add v0, 6
	ld	i,	fontP ; print character P
	call drawChar
	ret
drawChoice:
	; v0 -> x coordinate
	; v1 -> y coordinate
	ld va, #1 ; print 1
	call drawHex
	add v0, 10
	ld i, fontG ; print character G
	call drawChar
	add v0, 6
	ld va, #0 ; print character O
	call drawHex
	add v1, 6
	;newline
	ld v0, 20
	ld va, #2 ; print 2
	call drawHex
	add v0, 10
	ld va, #e ; print E
	call drawHex
	add v0, 6
	ld va, #d ; print D
	call drawHex
	ret
select:
	ld v0, k
	ld v3, 15 ; 0.25 seconds
	ld dt, v3
	call wait
	ret ;v0 = key pressed
wait:
	ld v3, dt
	se v3, 0
	jp wait
	ret
exec: ; GO
	cls
	jp #200
incIndex: ; 16 bit increment
	; v8 msb index
	; v9 lsb index
	ld v3, 2
	add v9, v3
	se vf, 1
	ret
	add v8, 1
	ret
setI: ; increment i to index v8 | v9
	ld i, #200
	add i, v9
	ld vb, 0
	sne vb, v8
	ret
setIloop:
	ld v3, #ff
	add i, v3
	ld v3, 1
	add i, v3 ; i = i + 256 for v8 times
	add vb, 1
	se vb, v8
	jp setIloop
	ret
main:
	ld v8, 0 ; msb index
	ld v9, 0 ; lsb index
	ld v0, 20
	ld v1, 12
	call drawLogo
	ld v3, 180 ; 3 seconds
	ld dt, v3
	call wait
inputChoice:
	cls
	ld v0, 20
	ld v1, 10
	call drawChoice
	call select
	sne v0, 1
	jp exec
	sne v0, 2
	jp editor
	jp inputChoice
	; start the editor
editor:
	cls
	ld v0, 2
	ld v1, 2
	ld vc, 0 ; counter digits, max 4 (2 byte opcode)
input:
	; print cursor
	ld i, cursor
	call drawChar
	ld va, k
keyloop:
	sknp va
	jp keyloop
	; erase cursor
	ld i, cursor
	call drawChar
	; print key
	call drawHex
	add v0, 6
	sne vc, 0
	ld v4, va
	sne vc, 1
	ld v5, va
	sne vc, 2
	ld v6, va
	sne vc, 3
	ld v7, va
nextKey:
	add vc, 1
	se vc, #4
	jp input
storeAndLoop:
	; v8 msb index
	; v9 lsb index
	; v4 | v5 | v6 | v7  four nibbles
	; store
	call setI
	ld v0, 0
	ld v1, 0
	shl v4, v4
	shl v4, v4
	shl v4, v4
	shl v4, v4
	or v0, v4
	or v0, v5
	shl v6, v6
	shl v6, v6
	shl v6, v6
	shl v6, v6
	or v1, v6
	or v1, v7
	ld [I], v1
	call incIndex
	jp inputChoice
fontP:
	db
	%11110000,
	%10010000,
	%11110000,
	%10000000,
	%10000000
fontG:
	db
	%11110000,
	%10000000,
	%10110000,
	%10010000,
	%11110000
cursor:
	db
	%11110000,
	%11110000,
	%11110000,
	%11110000,
	%11110000
